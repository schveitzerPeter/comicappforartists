import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserpageComponent } from './userpage.component';



@NgModule({
  declarations: [UserpageComponent],
  imports: [
    CommonModule
  ],exports:[UserpageComponent]
})
export class UserpageModule { }
