import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { ComicService } from '../_services/comic.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  

  

  constructor(   
    private router: Router,
    private comicService: ComicService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {    
  }

  public isLoggedIn() {            
    return this.authService.isLoggedin()
  }
  public logout(){
    this.authService.deleteToken()
    this.router.navigate(['/home'])
  }

  public randomComic(){            
    this.comicService.getRandomComicId().subscribe(
      (randomComicId)=>{                
        this.router.navigate(['/comic/' + randomComicId])
        .then(() => {
        window.location.reload();
        });
      }
    )     
  }
}
