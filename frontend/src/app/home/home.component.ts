import { Component, OnInit } from '@angular/core';
import { ComicService } from '../_services/comic.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from '../_services/auth.service';
import {map, Subscription, timer} from 'rxjs';  
import { interval } from 'rxjs';
import { switchMap, takeWhile } from 'rxjs/operators';
import { Router, RouterOutlet } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  popularUpdatedNew!:any
  
  timerSubscription!: Subscription; 


  constructor(
    private comicService: ComicService,
    public _sanitizer: DomSanitizer,
    public authService: AuthService,
    private router: Router
  ) {
    /*
    setInterval(() => {
      console.log("auth")
      if( this.authService.isLoggedin() ) {
        console.log("logged in")

        
        this.authService.validateCredentials().subscribe(
          (val)=>{
            console.log(val)
          },
          (err)=>{            
            if( err.status === 403){
              this.authService.deleteToken()
              this.router.navigate(['login'])
            }
          }
        )
        

      }
    }, 5000);
    */
  }

  ngOnInit(): void {
    this.comicService.popularUpdatedNew().subscribe(
      (data)=>{
        this.popularUpdatedNew=data
      }
    )    
  }

  szopdki(){
    console.log("auth")
    if(this.authService.isLoggedin()){
 
    }
  }
  
}
