import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagChipsComponent } from './tag-chips.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule} from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [TagChipsComponent],
  imports: [
    CommonModule,    
    MatFormFieldModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule

  ],exports:[
    TagChipsComponent
  ]
})
export class TagChipsModule { }
