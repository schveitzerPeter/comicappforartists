import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { ComicService } from '../_services/comic.service';
import { ElementRef, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {map, Observable, startWith} from 'rxjs';
import { TagService } from '../_services/tag.service';


@Component({
  selector: 'app-tag-chips',
  templateUrl: './tag-chips.component.html',
  styleUrls: ['./tag-chips.component.css']
})
export class TagChipsComponent implements OnInit {
  ngOnInit(): void {}
  
  separatorKeysCodes: number[] = [ COMMA];
  tagCtrl = new FormControl();
  filteredTags!: Observable<string[]>;
  tagsToDisplay: any[] = [];
  tagArrayForExport:any[] = [];
  allTagsThatMatchSubstring: any[] = [];

  
  @ViewChild('tagInput')
  fruitInput!: ElementRef<HTMLInputElement>;
  
  
  constructor(
    private tagService: TagService
  ) {     
    this.filteredTags = this.tagCtrl.valueChanges.pipe(      
      startWith(null),
      map((fruit: string | null) => (fruit ? this._filter(fruit) : this.allTagsThatMatchSubstring.slice())),
    );    
  }
  
  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();  
     
    // Add our fruit    
    if (value) {
      this.tagsToDisplay.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.tagCtrl.setValue(null);
  }
  
  remove(fruit: string): void {
    const index = this.tagsToDisplay.indexOf(fruit);
    
    if (index >= 0) {
      this.tagsToDisplay.splice(index, 1);
      this.tagArrayForExport.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {    
    const selectedTag = event.option.value

    
    let tagString = selectedTag.split("|")        
    this.tagArrayForExport.push(
      {tagid: tagString[1], name: tagString[0]}
    )    
    this.tagsToDisplay.push(tagString[0]);
    this.fruitInput.nativeElement.value = '';
    this.tagCtrl.setValue(null);
  }
  
  private _filter(value: string): any[] {
    let tagsFromBackend: any[] = [];
    this.getTags(value).subscribe(
      (value: string | any[]) => {
        for(let i = 0; i<value.length; i++){          
          tagsFromBackend.push(value[i].tagname + "|" + value[i].tagid)
        }
      }
    )           
    console.log(tagsFromBackend)
    return tagsFromBackend
  }    
  
  public getTags( filter:string): Observable<any> {
    return this.tagService.getTagBySubstring(filter)
  }
  
  public showSelectedTags(){
    this.tagArrayForExport.forEach(function (arrayItem) {      
      console.log(arrayItem);
  }); 
  }
}
