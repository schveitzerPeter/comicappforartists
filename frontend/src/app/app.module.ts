import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TagChipsModule } from './tag-chips/tag-chips.module';
import { SearchModule } from './search/search.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ErrorPageComponent } from './error-page/error-page.component';

import { MycomiclistComponent } from './mycomiclist/mycomiclist.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { ComicComponent } from './comic/comic.component';
import { ChapterComponent } from './chapter/chapter.component';
import { SavecomicComponent } from './savecomic/savecomic.component';
import { AddchapterComponent } from './addchapter/addchapter.component';

import { UserpageModule } from './userpage/userpage.module';
import { HeaderModule } from './header/header.module';
import { LoginModule } from './login/login.module';
import { RegisterModule } from './register/register.module';
import { HomeModule } from './home/home.module';
import { ErrorPageModule } from './error-page/error-page.module';
import { MycomiclistModule } from './mycomiclist/mycomiclist.module';
import { BookmarksModule } from './bookmarks/bookmarks.module';
import { ComicModule } from './comic/comic.module';
import { ChapterModule } from './chapter/chapter.module';
import { SavecomicModule } from './savecomic/savecomic.module';
import { AddchapterModule } from './addchapter/addchapter.module';
import { ComicService } from './_services/comic.service';
import { HttpClientModule } from '@angular/common/http';
import { ComicreaderModule } from './comicreader/comicreader.module';
import { AuthGuardService } from './_guard/aut-guard.service';
import { BookmarkdialogComponent } from './bookmarkdialog/bookmarkdialog.component';
import { AddcomicComponent } from './addcomic/addcomic.component';
import { AddcomicModule } from './addcomic/addcomic.module';
import { PagecontainerComponent } from './pagecontainer/pagecontainer.component';
import { PagecontainerModule } from './pagecontainer/pagecontainer.module';





@NgModule({
  declarations: [
    AppComponent,
    BookmarkdialogComponent,    
  ],
  imports: [
    TagChipsModule,
    SearchModule,
    UserpageModule,
    HeaderModule,
    LoginModule,
    RegisterModule,
    HomeModule,
    ErrorPageModule,
    MycomiclistModule,
    BookmarksModule,
    ComicModule,
    ChapterModule,
    SavecomicModule,
    AddchapterModule,
    HttpClientModule,
    ComicreaderModule,
    BookmarksModule,
    MycomiclistModule,
    AddcomicModule,
    PagecontainerModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
