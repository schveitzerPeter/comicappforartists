import { Injectable } from '@angular/core';
import { Router, CanActivate  } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private authService: AuthService,
        public router: Router
    ){}

    canActivate() {
        if(!this.authService.isLoggedin()){
            this.router.navigate(['login'])
            return false
        }
        return true
    }
    
}