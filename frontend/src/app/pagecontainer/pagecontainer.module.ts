import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagecontainerComponent } from './pagecontainer.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';




@NgModule({
  declarations: [PagecontainerComponent],
  imports: [
    CommonModule,
    MatExpansionModule,
    DragDropModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,    
    
    
  ],exports:[
    PagecontainerComponent
  ]
})
export class PagecontainerModule { }
