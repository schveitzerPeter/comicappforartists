import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { FormControl, Validators } from '@angular/forms';


export class Page {

  private hash
  private fileName

  constructor(hash:string, fileName:string) {
    this.hash = hash;
    this.fileName=fileName
  }

  getHash(){
    return this.hash
  }

  getFileName(){
    return this.fileName

  }
}
@Component({
  selector: 'app-pagecontainer',
  templateUrl: './pagecontainer.component.html',
  styleUrls: ['./pagecontainer.component.css']
})

export class PagecontainerComponent implements OnInit {
  

  constructor() {}    
  ngOnInit(): void {
     //this.chapterTitle.setValue('')          
     
  }


  @Input() data: any;
  @Input() chapterId: any
  @Input() pageArray:Page[]=[]  
  @Input() chapterTitle = new FormControl('', {
    validators:[Validators.required, ]    
  })
  

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.pageArray, event.previousIndex, event.currentIndex);              
  }

  onFileSelected(event:any){
    if(event.target.files){            
      for(let i = 0; i < event.target.files.length; i++){
        let fileReader = new FileReader()        
        fileReader.readAsDataURL(event.target.files[i])        
        let fileName:string = event.target.files[i].name
        fileReader.onload=(event:any)=>{
          this.pageArray.push(
            new Page(event.target.result, fileName)
          )          
        }           
      }
      
    }
  }

  deletePage( indexInArray:number){
    this.pageArray.splice(indexInArray, 1)    
  }

  
}