import { Component, Inject, OnInit } from '@angular/core';
import {  MatDialogRef} from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { BookmarkService } from '../_services/bookmark.service';




@Component({
  selector: 'app-bookmarkdialog',
  templateUrl: './bookmarkdialog.component.html',
  styleUrls: ['./bookmarkdialog.component.css']
})
export class BookmarkdialogComponent implements OnInit {

  constructor(    
    public dialogRef: MatDialogRef<BookmarkdialogComponent>, @Inject(MAT_DIALOG_DATA) public data: {comicId: string, chapterId:string, pageId:string},
    private bookmarkService: BookmarkService
  ) { }

  ngOnInit(): void {
  }

  add(){
    const body = {
      comicId: this.data.comicId,
      chapterId: this.data.chapterId,
      pageId: this.data.pageId.toString()
    }    
    this.bookmarkService.addBookmark(body).subscribe(
      (val)=>{
        //todo 

      }
    )
    this.dialogRef.close()
  }

  close(){
    this.dialogRef.close()
  }
}
