import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkdialogComponent } from './bookmarkdialog.component';

describe('BookmarkdialogComponent', () => {
  let component: BookmarkdialogComponent;
  let fixture: ComponentFixture<BookmarkdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookmarkdialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
