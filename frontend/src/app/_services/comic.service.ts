import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toBase64String } from '@angular/compiler/src/output/source_map';


@Injectable({
  providedIn: 'root'
})
export class ComicService {

  API_PATH = "http://localhost:8080/api/v1/comic/";

  constructor(
    private http: HttpClient
  ) { }

  public getTagBySubstring( tagSubstring:string) {
    return this.http.post(this.API_PATH + 'taglist', {'tag':tagSubstring} )
  }

  public getComicById( comicId:string){    
    return this.http.get(this.API_PATH + comicId)
  }

  public getRandomComicId(){
    return this.http.get(this.API_PATH + 'random')
  }

  public popularUpdatedNew(){
    return this.http.get(this.API_PATH + 'popularUpdatedNew')
  }

  public getChapterPagesByChapterId( chapterId:string) {
    return this.http.get(this.API_PATH + 'pages/'+ chapterId)
  }

  public getChaptersByComicId(comicId: string) {
    return this.http.get(this.API_PATH + 'chapters/'+ comicId)
  }

  public searchComics( searchFormValue:any, page:string, size:string){
    return this.http.post(this.API_PATH + 'search/'+page+'/'+size, searchFormValue)
  }

  public addComic( comicFormValue: any,){
    const jwt = localStorage.getItem('jwt')
    const headers = { 'Authorization': ''+jwt };
    return this.http.post(this.API_PATH+'save',comicFormValue, {headers})
  }
  public getUserComic(){
    const jwt = localStorage.getItem('jwt')
    const headers = { 'Authorization': ''+jwt };
    return this.http.get(this.API_PATH+'usercomic', {headers})
  }

  public deleteUserComic(id:string){
    const jwt = localStorage.getItem('jwt')
    const headers = { 'Authorization': ''+jwt };
    return this.http.delete(this.API_PATH+'deletecomic/'+id, {headers})
  }

  public editUserComic(id:string, comicFormValue:any){
    const jwt = localStorage.getItem('jwt')
    const headers = { 'Authorization': ''+jwt };
    return this.http.post(this.API_PATH+'save',comicFormValue, {headers})
  }

  public getUserComicDataForEditing(id:string){
    const jwt = localStorage.getItem('jwt')
    const headers = { 'Authorization': ''+jwt };
    return this.http.get(this.API_PATH+'edit/'+id, {headers})
  }

  public editComic(comicValue:any,id:string){
    const jwt = localStorage.getItem('jwt')
    const headers = { 'Authorization': ''+jwt };
    return this.http.post(this.API_PATH+'edit/'+id, comicValue, {headers})
  }
}
