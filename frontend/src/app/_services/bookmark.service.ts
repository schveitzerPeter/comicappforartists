import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

    constructor(
        private http: HttpClient
    ){}

    API_PATH = "http://localhost:8080/api/v1/bookmark/";

    public getBookmarks(){
        const jwt = localStorage.getItem('jwt')
        return this.http.get(this.API_PATH,{
            headers: {'Authorization':''+jwt}
        })
    }

    public deleteBookmark(comicId:any){
        const jwt = localStorage.getItem('jwt')
        return this.http.delete(this.API_PATH + comicId,{
            headers: {'Authorization':''+jwt}
        })
    }

    public addBookmark( bookmark: any ) {
        const jwt = localStorage.getItem('jwt')
        const headers = { 'Authorization': ''+jwt };
        return this.http.post(this.API_PATH, bookmark, {headers} )
    }
}