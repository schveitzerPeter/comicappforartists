import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TagService {

  API_PATH = "http://localhost:8080/api/v1/tag/";

  constructor(
    private http: HttpClient
  ) { }

  public getTagBySubstring( tagSubstring:string) {
    return this.http.post(this.API_PATH + 'taglist/'+tagSubstring, {})
  }

}
