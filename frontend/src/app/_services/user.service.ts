import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, delay, map, Observable, of, Subject } from 'rxjs';
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  API_PATH = "http://localhost:8080/api/v1/user/";

  constructor(
    private http: HttpClient
  ) { }

  
  public checkIfEmailExistInDatabase( _email:string) {    
    return this.http.post(this.API_PATH + 'checkemail', {email: _email})
  }

  public register( registerForm:any) {
    return this.http.post(this.API_PATH + 'register', registerForm)
  }

  public login( loginForm:any){
    return this.http.post(this.API_PATH + 'login', loginForm)
  }

  uniqueEmailValidator(): AsyncValidatorFn{
    return (control: AbstractControl): Observable<ValidationErrors | null> =>{
      return this.checkIfEmailExistInDatabase(control.value).pipe(
        map( responseEmail =>           
          (responseEmail.toString() === 'true' ? {emailExist: true} : null)
        ),
        catchError(async (error) => null)
      )
    }
  }
}
