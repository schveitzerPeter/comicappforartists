import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
    API_PATH = "http://localhost:8080/api/v1/user/";
    constructor(
        private http: HttpClient
    ){}

    deleteToken(){
        localStorage.removeItem('jwt')
    }

    isLoggedin(){
        return (localStorage.getItem('jwt'))
    }

    setToken( token: string){
        localStorage.setItem('jwt',token)
    }

    validateCredentials(){
        const jwt = localStorage.getItem('jwt')
        const headers = { 'Authorization': ''+jwt };    
        return this.http.get( this.API_PATH + 'auth',{headers})
    }
}