import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private routerService: Router
  ) { }
  loginForm: FormGroup = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    }
  )

  cred:boolean = false

  ngOnInit(): void {
  }
  login(){
    console.log(this.loginForm)
    if(this.loginForm.valid){
      this.userService.login( this.loginForm.value ).subscribe(
        (val:any) => {
          this.authService.setToken(val.token)
          this.routerService.navigate(['home'])
        },
        (err:any)=>{          
          if( err.status == 403){
            this.cred = true
            console.log(this.cred)
          }
        }
      )
      
    }
  }
  fillCred(){
    this.loginForm.patchValue({
      email:"schveitzer.peter@gmail.com",
      password:"AdoraxCatra_S05E13"
    })
  }
}
