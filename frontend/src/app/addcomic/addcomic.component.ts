import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, ComponentFactoryResolver, Input, OnInit, QueryList, ViewChild, ViewChildren, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Page, PagecontainerComponent } from '../pagecontainer/pagecontainer.component';
import { TagChipsComponent } from '../tag-chips/tag-chips.component';
import { ComicService } from '../_services/comic.service';

@Component({
  selector: 'app-addcomic',
  templateUrl: './addcomic.component.html',
  styleUrls: ['./addcomic.component.css']
})
export class AddcomicComponent implements OnInit {

  constructor(
    private comicServcie: ComicService,
    private route: ActivatedRoute,
    private router: Router
  ) { }
  
  
  chapterArray: PagecontainerComponent[] = [];
  
  @ViewChild('includedTagChips') includedTagChips!: TagChipsComponent;
  comicState = new FormControl();
  comicTitle = new FormControl();
  
  @ViewChildren('comicChapters') comicChapters!: QueryList<PagecontainerComponent>;
  buttonText:string = 'Add Comic'
  comicId:any = null
  ngOnInit(): void {
    this.comicId = this.route.snapshot.paramMap.get('comicId');
    if( this.comicId ){
      this.buttonText = 'Edit Comic'
      this.comicServcie.getUserComicDataForEditing(this.comicId).subscribe(
        (val:any)=>{                
          let tagArray:any[] = val.tagArray
          for(let i = 0; i < tagArray.length; i++){
            this.includedTagChips.tagsToDisplay.push(
              tagArray[i].name
            )
          }
          

          this.includedTagChips.tagArrayForExport = val.tagArray
          
          this.comicTitle.setValue(val.comicTitle)        
          this.comicState.setValue(val.status.toString())
          
          let chapterArray: any[] = val.chapterArray
          for( let i = 0; i < chapterArray.length; i++) {
            let pageContainerTmp = new PagecontainerComponent()
            pageContainerTmp.chapterId = chapterArray[i].chapterId
            pageContainerTmp.chapterTitle.setValue(chapterArray[i].chapterTitle)
            let chapterPageArray: any[] = chapterArray[i].pageArray
            for( let j = 0; j < chapterPageArray.length; j++){
              pageContainerTmp.pageArray.push(
                new Page(chapterPageArray[j].hash, "")   
              )           
            }            
            this.chapterArray.push(pageContainerTmp)
          }          
        },
        (err)=>{
          //TODO Reroute
        }        
      )      
    }        
  }

  drop(event: CdkDragDrop<PagecontainerComponent[]>) {
    moveItemInArray(this.chapterArray, event.previousIndex, event.currentIndex);
  }

  addChapter(){        
    this.chapterArray.push( new PagecontainerComponent())        
  }

  deleteChapter(indexInArray:number){
    //console.log(this.chapterArray[indexInArray].pageContainerId)
    this.chapterArray.splice(indexInArray, 1)  
  }

  addComic(){
    let chapterArray:any[] = []
    let queryListTo:any[] = this.comicChapters.toArray()
    for(let i = 0; i < queryListTo.length; i++ ){
      console.log(queryListTo[i])
      let tmp_chapter = {
        name: queryListTo[i].chapterTitle.value,
        page: queryListTo[i].pageArray,
        chapterid: queryListTo[i].chapterId
      }
      chapterArray.push(tmp_chapter)
    }
    const comic = {
      comicTitle: this.comicTitle.value,
      state: this.comicState.value,
      tags: this.includedTagChips.tagArrayForExport,
      chapters: chapterArray
    }     
    console.log(comic)
    
    if(this.comicId != null){            
      this.comicServcie.editComic(comic,this.comicId).subscribe( (val)=>{
        console.log(val)
        this.router.navigate(['mycomics'])
      })
    }else{
      this.comicServcie.addComic(comic).subscribe( (val)=>{
        console.log(val)
        this.router.navigate(['mycomics'])
      })                  
    }      
  }
}