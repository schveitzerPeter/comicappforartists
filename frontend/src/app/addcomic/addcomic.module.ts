import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddcomicComponent } from './addcomic.component';
import { TagChipsModule } from '../tag-chips/tag-chips.module';
import { MatExpansionModule} from '@angular/material/expansion';
import { PagecontainerModule } from '../pagecontainer/pagecontainer.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [AddcomicComponent],
  imports: [
    CommonModule,
    TagChipsModule,
    PagecontainerModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    DragDropModule,
    RouterModule
  ],exports:[
    AddcomicComponent
  ]
})
export class AddcomicModule { }
