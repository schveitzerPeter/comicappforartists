import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddchapterComponent } from './addchapter.component';



@NgModule({
  declarations: [AddchapterComponent],
  imports: [
    CommonModule
  ],exports:[
    AddchapterComponent
  ]
})
export class AddchapterModule { }
