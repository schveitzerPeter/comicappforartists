import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MycomiclistComponent } from './mycomiclist.component';

describe('MycomiclistComponent', () => {
  let component: MycomiclistComponent;
  let fixture: ComponentFixture<MycomiclistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MycomiclistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MycomiclistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
