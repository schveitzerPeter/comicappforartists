import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ComicService } from '../_services/comic.service';

@Component({
  selector: 'app-mycomiclist',
  templateUrl: './mycomiclist.component.html',
  styleUrls: ['./mycomiclist.component.css']
})
export class MycomiclistComponent implements OnInit {
  myComics:any
  constructor(
    private comicService: ComicService,
    private routerService: Router
  ) { }

  ngOnInit(): void {
    this.loadComic()  
  }
  public loadComic(){
    this.comicService.getUserComic().subscribe(
      myComicList => { this.myComics = myComicList },
      (err) => { 
        if(err.code === 403){
          localStorage.removeItem('jwt')
          this.routerService.navigate(['login'])
        }
      }
    )
  }

  public deleteComic(id:string){
    this.comicService.deleteUserComic(id).subscribe(
      (err:any) => { 
        if(err.code === 403){
          localStorage.removeItem('jwt')
          this.routerService.navigate(['login'])
        }
      },
      ()=>{
        this.loadComic()
      }
    )
  }

  public editComic(id:string){
    this.routerService.navigate(['editcomic/'+id])
  }

  public addComicNav() {
    this.routerService.navigate(['addcomic'])
  }
}
