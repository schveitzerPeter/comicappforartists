import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MycomiclistComponent } from './mycomiclist.component';



@NgModule({
  declarations: [MycomiclistComponent],
  imports: [
    CommonModule
  ],exports:[MycomiclistComponent]
})
export class MycomiclistModule { }
