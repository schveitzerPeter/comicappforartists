import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddcomicComponent } from './addcomic/addcomic.component';
import { AppComponent } from './app.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { ChapterComponent } from './chapter/chapter.component';
import { ComicComponent } from './comic/comic.component';
import { ComicreaderComponent } from './comicreader/comicreader.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MycomiclistComponent } from './mycomiclist/mycomiclist.component';
import { PagecontainerComponent } from './pagecontainer/pagecontainer.component';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { TagChipsComponent } from './tag-chips/tag-chips.component';
import { 
  AuthGuardService as AuthGuard 
} from './_guard/aut-guard.service';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'search', component: SearchComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'chips', component: TagChipsComponent },
  { path: 'comic/:comidId', component: ComicComponent },
  { path: 'comic/:comidId/:chapterId', component: ChapterComponent },
  { path: 'comicreader/:comidId/:chapterId', component: ComicreaderComponent },
  { path: 'comicreader/:comidId/:chapterId/:pageId', component: ComicreaderComponent },  
  { 
    path: 'bookmarks',
    component: BookmarksComponent,
    canActivate: [AuthGuard] 
  },
  { path: 'mycomics', component: MycomiclistComponent },  
  { path: 'addcomic', component: AddcomicComponent },
  { path: 'pagecontainer', component: PagecontainerComponent },
  { path: 'editcomic/:comicId', component: AddcomicComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
