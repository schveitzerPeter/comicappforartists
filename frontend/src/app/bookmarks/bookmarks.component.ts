import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { BookmarkService } from '../_services/bookmark.service';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {

  bookmarks:any  

  constructor(
    private bookmarkService: BookmarkService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadBookmarks()
  }

  deleteBookmarks(bookmarkId:any){
    console.log("delete bookmark with id: " + bookmarkId)
    this.bookmarkService.deleteBookmark( bookmarkId ).subscribe(
      (val:any)=>{                
        this.loadBookmarks()
      }
    )
    
  }

  loadBookmarks(){
    this.bookmarkService.getBookmarks().subscribe(
      (val:any)=>{        
        this.bookmarks = val
      },
      (err)=>{
        if(err.status===403){
          this.authService.deleteToken()
          this.router.navigate(['login'])
        }
      }
    )   
  }
}
