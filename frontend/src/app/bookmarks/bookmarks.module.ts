import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookmarksComponent } from './bookmarks.component';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  declarations: [BookmarksComponent],
  imports: [
    CommonModule,
    MatIconModule
  ],exports:[
    BookmarksComponent
  ]
})
export class BookmarksModule { }
