import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SavecomicComponent } from './savecomic.component';



@NgModule({
  declarations: [SavecomicComponent],
  imports: [
    CommonModule
  ],exports:[
    SavecomicComponent
  ]
})
export class SavecomicModule { }
