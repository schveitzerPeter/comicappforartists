import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavecomicComponent } from './savecomic.component';

describe('SavecomicComponent', () => {
  let component: SavecomicComponent;
  let fixture: ComponentFixture<SavecomicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavecomicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavecomicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
