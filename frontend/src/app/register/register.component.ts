import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  ngOnInit(): void {}  
  constructor(
    private userService: UserService,
    private routerService: Router
  ){}

  registerForm = new FormGroup({
      email: new FormControl('', {
        validators:[Validators.required, Validators.email],
        asyncValidators:[this.userService.uniqueEmailValidator()]
      }),
      password: new FormControl('', [   
        Validators.required,
        Validators.pattern('^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{12,}$')

      ]),
      confirmPassword: new FormControl('', [        
        Validators.required,
        Validators.pattern('^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{12,}$')
      ]),
    },
    {
      validators: this.passwordMatcher('password', 'confirmPassword')
    }
  );  
  

  passwordMatcher(pass1:string, pass2:string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null =>{
      const formGroup = control as FormGroup
      const pass_1 = formGroup.get(pass1)?.value
      const pass_2 = formGroup.get(pass2)?.value

      if( pass_1 === pass_2){
        return null
      } else {
        return { passDontMatch: true}
      }
    }
  }

  registerDTO: any
  setRegisterDTO(){
    this.registerDTO = {
      email: this.registerForm.value.email,
      password: this.registerForm.value.password
    }
  }
  register(){    
    this.setRegisterDTO()
    if(this.registerForm.valid){      
      this.userService.register(this.registerDTO).subscribe(
        (response)=>{
          this.routerService.navigate(['login'])
        },
        (error: HttpErrorResponse) => {
         
        }
      )
    }
  }
}