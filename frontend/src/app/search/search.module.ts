import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { TagChipsModule } from '../tag-chips/tag-chips.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { ReactiveFormsModule} from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button'
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';




@NgModule({
  declarations: [SearchComponent],
  imports: [    
    CommonModule,
    TagChipsModule,    
    MatExpansionModule,    
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule 

  ],
  providers:[
    MatDatepickerModule
  ],
  exports:[SearchComponent]
})
export class SearchModule { }
