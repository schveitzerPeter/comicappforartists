import { Component, OnInit, ViewChild, AfterContentInit, AfterViewInit } from '@angular/core';
import { TagChipsComponent } from '../tag-chips/tag-chips.component';
import { FormControl } from '@angular/forms';
import { ComicService } from '../_services/comic.service';
import { from } from 'rxjs';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements AfterViewInit {

  @ViewChild('includedTagChips') includedTagChips!: TagChipsComponent;
  @ViewChild('excludedTagChips') excludedTagChips!: TagChipsComponent;
  @ViewChild('matPaginator') matPaginator!: MatPaginator;
  includedTags:any
  excludedTags:any

  viewCountFrom = new FormControl();
  viewCountTo = new FormControl();
  comicState = new FormControl();

  pageCountFrom = new FormControl();
  pageCountTo = new FormControl();
  orderBy = new FormControl();

  fromDate = new FormControl();
  toDate = new FormControl();
  pageSize = new FormControl();

  searchFormValue: any;

  comicListFromBackend?: any;

  constructor(
    private comicService: ComicService
  ) { }

  ngAfterViewInit(): void {this.includedTags = this.includedTagChips.tagArrayForExport      }
  ngOnInit(): void {}



  OnPageChange(event: PageEvent){
    this.setSearchForm();
    this.comicService.searchComics( this.searchFormValue, event.pageIndex.toString(), event.pageSize.toString()  ).subscribe(
      (searchResult)=>{        
        this.comicListFromBackend = searchResult
      }
    )
  }

  search() {
    this.setSearchForm();
    this.comicService.searchComics( this.searchFormValue, '0','10' ).subscribe(
      (searchResult)=>{        
        this.comicListFromBackend = searchResult
      }
    )
 }

 setSearchForm() {
  this.searchFormValue = {
    includedTags : this.includedTagChips.tagArrayForExport,      
    viewCountFrom: this.viewCountFrom.value,
    viewCountTo: this.viewCountTo.value,
    pageCountFrom: this.pageCountFrom.value,
    pageCountTo: this.pageCountTo.value,
    status: this.comicState.value,
    orderBy: this.orderBy.value,
    fromDate: this.fromDate.value,
    toDate: this.toDate.value      
  }
 }
}
