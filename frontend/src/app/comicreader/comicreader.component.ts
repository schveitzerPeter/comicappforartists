import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, DoCheck, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectableObservable } from 'rxjs';
import { ComicService } from '../_services/comic.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { BookmarkdialogComponent } from '../bookmarkdialog/bookmarkdialog.component';
import { AuthService } from '../_services/auth.service';


@Component({
  selector: 'app-comicreader',
  templateUrl: './comicreader.component.html',
  styleUrls: ['./comicreader.component.css']
})
export class ComicreaderComponent implements OnInit,  AfterViewChecked {

  chapterArray:any[] = []
  lastChapter!:boolean
  firstChapter!:boolean
  chapters!:any
  pages!:any;
  comicId: string = ""
  chapterId: string = ''
  pageId!:any
  selectedChapter="SelectedChapter"
  scroll:boolean =true

  previuesChapterId!:any
  nextChapterId!:any

  constructor(
    private comicService: ComicService,
    private activatedRoute: ActivatedRoute,        
    private dialog: MatDialog,
    private authService: AuthService,
    private routerService: Router
    ) { }
  
    
    openDialog( comicId:string, chapterId:string, pageId:string) {
      if( this.authService.isLoggedin() ) {
        this.dialog.open(BookmarkdialogComponent,{
          width: '250px',
          data:{ 
            "comicId": comicId,
            "chapterId": chapterId,
            "pageId": pageId
          }
        })
      }            
    }
  
    ngAfterViewChecked(): void {
    let htmlElement = document.getElementById(this.pageId)
    if(htmlElement && this.scroll){      
      this.scroll= false
      htmlElement.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'})
    }      
  }

  

  ngOnInit(): void {
    console.log(this.chapterArray)

    this.comicId = this.activatedRoute.snapshot.params['comidId']
    this.chapterId = this.activatedRoute.snapshot.params['chapterId']
    this.pageId = this.activatedRoute.snapshot.params['pageId']    
    
        
    this.comicService.getChaptersByComicId(this.comicId).subscribe(
      (chapters)=>{
        this.chapters = JSON.parse(JSON.stringify(chapters));   
        this.chapters.forEach((element: any) => {
          this.chapterArray.push(element)          
        });
        if(this.chapterArray.length==1){
            this.lastChapter = true
            this.firstChapter = true
        }
        let currentChapterId:number = 0
        for(let i = 0; i < this.chapterArray.length; i++){
          if( this.chapterId == this.chapterArray[i].chapterId ){
            currentChapterId = i
          }
        }

        this.selectedChapter=this.chapterArray[currentChapterId].chapterTitle
        console.log(this.selectedChapter)
        if( this.chapterArray[currentChapterId-1] != undefined ){
          this.nextChapterId = this.chapterArray[currentChapterId-1].chapterId
        } else {
          this.lastChapter = true          
        }

        if( this.chapterArray[currentChapterId+1] != undefined ){
          this.previuesChapterId = this.chapterArray[currentChapterId+1].chapterId
        }else{
          this.firstChapter = true          
        }
        
        

        
        
      }
    )
    
    

    this.comicService.getChapterPagesByChapterId(this.chapterId).subscribe(
      (pages)=>{
        this.pages = pages
      }
    )    
  }    

  navigateTo(  comicId:string, chapterId:string ){
    this.routerService.navigate(['comicreader/'+comicId+'/'+chapterId]).then(() => {
      window.location.reload();
      });
  }

}