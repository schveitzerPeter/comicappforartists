import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComicreaderComponent } from './comicreader.component';
import {MatSelectModule} from '@angular/material/select';
import { RouterModule } from '@angular/router';
import {MatDialogModule} from "@angular/material/dialog";




@NgModule({
  declarations: [ComicreaderComponent],
  imports: [
    CommonModule,
    MatSelectModule,
    RouterModule,
    MatDialogModule
  ],exports:[
    ComicreaderComponent
  ]
})
export class ComicreaderModule { }
