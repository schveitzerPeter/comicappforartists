import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComicreaderComponent } from './comicreader.component';

describe('ComicreaderComponent', () => {
  let component: ComicreaderComponent;
  let fixture: ComponentFixture<ComicreaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComicreaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComicreaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
