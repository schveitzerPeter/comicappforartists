import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComicComponent } from './comic.component';
import { ActivatedRoute } from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';



@NgModule({
  declarations: [ComicComponent],
  imports: [
    CommonModule,    
    MatExpansionModule 
  ],exports:[
    ComicComponent
  ]
})
export class ComicModule { }
