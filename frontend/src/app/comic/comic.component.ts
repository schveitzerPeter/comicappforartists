import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ComicService } from '../_services/comic.service';


@Component({
  selector: 'app-comic',
  templateUrl: './comic.component.html',
  styleUrls: ['./comic.component.css']
})
export class ComicComponent implements OnInit {

  comicId!: string;
  panelOpenState = false;
  comicData!: any
  expansonPanelChapter!:any
  pages!: any
  constructor(
    private activatedRoute: ActivatedRoute,
    private comicService: ComicService
  ) { }

  ngOnInit() {
    this.comicId = this.activatedRoute.snapshot.params['comidId']

    this.comicService.getComicById( this.comicId ).subscribe(
      (data)=>{
        this.comicData = data
        console.log(this.comicData)
      }
    )
   
  }

  public listPages( chapterId:string ){
    console.log("chapterId: " + chapterId)
    this.comicService.getChapterPagesByChapterId(chapterId).subscribe(
      (pages)=>{
        console.log(pages)
        this.pages = pages
      }
    )
  }

}
