import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.css']
})
export class ChapterComponent implements OnInit {

  comicId!:string
  chapterId!:string
  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.comicId = this.activatedRoute.snapshot.params['comidId']
    this.chapterId = this.activatedRoute.snapshot.params['chapterId']
    console.log( this.comicId );
    console.log( this.chapterId );
    
  }

}
