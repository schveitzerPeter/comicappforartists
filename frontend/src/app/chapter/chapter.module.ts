import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChapterComponent } from './chapter.component';



@NgModule({
  declarations: [ChapterComponent],
  imports: [
    CommonModule
  ],exports:[
    ChapterComponent
  ]
})
export class ChapterModule { }
