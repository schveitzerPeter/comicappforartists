package hu.schveitzer.peter.ComicReader.Controller;

import hu.schveitzer.peter.ComicReader.DTO.BookmarkDTO;
import hu.schveitzer.peter.ComicReader.Model.Bookmark;
import hu.schveitzer.peter.ComicReader.Model.User;
import hu.schveitzer.peter.ComicReader.Service.BookmarkService;
import hu.schveitzer.peter.ComicReader.Service.ComicService;
import hu.schveitzer.peter.ComicReader.Service.UserService;
import hu.schveitzer.peter.ComicReader.Util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.InvalidClaimException;
import io.jsonwebtoken.SignatureException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/bookmark/")
@CrossOrigin(origins = "http://localhost:4200", methods = {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE})
public class BookmarkController {

    @Autowired
    BookmarkService bookmarkService;

    @Autowired
    UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private ComicController comicController;


    public void setBookmarkService(BookmarkService bookmarkService) {
        this.bookmarkService = bookmarkService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/", consumes = "application/json", method = RequestMethod.POST)
    public ResponseEntity<String> getBookmarksByUser( @RequestHeader("Authorization") String jwtToken,@RequestBody BookmarkDTO bookmark ) throws JSONException {

        User user = userService.getUserFromJwtToken(jwtToken);
        if( user == null ) {
            return ResponseEntity.status(403).build();
        }


        Bookmark bookmarkToSave = new Bookmark();
        bookmarkToSave.setComicid( bookmark.getComicId() );
        bookmarkToSave.setChapterid( bookmark.getChapterId());
        bookmarkToSave.setPageid( bookmark.getPageId());


        user.setUserid(user.getUserid());
        bookmarkToSave.setUser(user);
        Bookmark newBookmark = bookmarkService.saveBookmark(bookmarkToSave);


        JSONObject response = new JSONObject();
        response.put("msg", "Bookmark with id: "+newBookmark.getBookmarkid()+ "deleted");
        return ResponseEntity.ok(response.toString());
    }

    @RequestMapping(value = "/{bookmarkId}",  method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteBookmarkById(
        @RequestHeader("Authorization") String jwtToken,
        @PathVariable(value = "bookmarkId") Long bookmarkId
    ) throws JSONException {
        if( userService.bookmarkBelongsToUser(bookmarkId, jwtToken) ) {
            bookmarkService.deleteBookmarkById(bookmarkId);
            JSONObject response = new JSONObject();
            response.put("msg", "Bookmark with id: "+bookmarkId+ "deleted");
            return ResponseEntity.ok(response.toString());
        }
        return ResponseEntity.status(403).build();
    }

    @RequestMapping(value = "/",  method = RequestMethod.GET)
    public ResponseEntity<String> getBookmarksByUser( @RequestHeader("Authorization") String jwtToken)  {
        User user = userService.getUserFromJwtToken(jwtToken);
        if( user == null ) {
            return ResponseEntity.status(403).build();
        }
        JSONArray bookmarkJson = new JSONArray();
        this.bookmarkService.getBookmarksByUserid(user.getUserid()).stream().forEach( bookmark->{
            JSONObject bookmarJsonArrayElement = new JSONObject();
            try {
                JSONObject a = this.comicController.getCoverArtById(bookmark.getComicid());
                bookmarJsonArrayElement.put("bookmarkId", bookmark.getBookmarkid());
                bookmarJsonArrayElement.put("title", a.get("title"));
                bookmarJsonArrayElement.put("comicId", bookmark.getComicid());
                bookmarJsonArrayElement.put("chapterId", bookmark.getChapterid() );
                bookmarJsonArrayElement.put("pageId", bookmark.getPageid() );
                bookmarJsonArrayElement.put("cover", a.get("coverart") );
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            bookmarkJson.put(bookmarJsonArrayElement);
        });
        return ResponseEntity.ok(bookmarkJson.toString());
    }
}