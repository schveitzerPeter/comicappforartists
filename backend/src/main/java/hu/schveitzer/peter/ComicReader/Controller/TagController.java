package hu.schveitzer.peter.ComicReader.Controller;



import hu.schveitzer.peter.ComicReader.Model.Tag;
import hu.schveitzer.peter.ComicReader.Service.TagService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/tag/")
@CrossOrigin(origins = "http://localhost:4200" , methods = {RequestMethod.POST, RequestMethod.GET} )
public class TagController {

    TagService tagService;

    @Autowired
    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    @RequestMapping(value = "{id}", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<String> getTagById(@PathVariable(value = "id") Long tagId) throws JSONException {
        Tag tag = this.tagService.findById(tagId).get();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id",tag.getTagid());
        jsonObject.put("name",tag.getTagname());

        return ResponseEntity.ok(jsonObject.toString());
    }

    @RequestMapping(value = "taglist/{substring}", produces = "application/json", method = RequestMethod.POST)
    public ResponseEntity<String> getTagsBySubstring(@PathVariable(value = "substring") String subString){

        JSONArray tagArrayJson = new JSONArray();
        try{
            this.tagService.findTagByTagnameContaining(subString).stream().forEach(x->{
                JSONObject tagJson = new JSONObject();
                try {
                    tagJson.put("tagid", x.getTagid());
                    tagJson.put("tagname", x.getTagname());
                } catch (JSONException e) {

                }
                tagArrayJson.put(tagJson);
            });
            return ResponseEntity.ok(tagArrayJson.toString());
        } catch ( Exception e){
            return ResponseEntity.status(500).body("Something went wrong");
        }
    }

    @RequestMapping(value = "savetag", produces = "application/json", method = RequestMethod.POST, consumes ="application/json")
    public ResponseEntity<String> saveTag( @RequestBody Tag tag) {
        System.out.println(tag);
        try {
            if(!tag.getTagname().equals(null)){
                this.tagService.saveTag( tag );
            } else {
                return ResponseEntity.ok("Incorrect dataformat");
            }
        }catch (Exception e){
            return ResponseEntity.status(500).body("Something went wrong");
        }
        return ResponseEntity.ok(tag.getTagname());
    }



}