package hu.schveitzer.peter.ComicReader.DTO;

import lombok.*;
@AllArgsConstructor
@Getter
@Setter
@ToString
@Data
public class UserRegistrationDTO {
    private String email;
    private String password;
}
