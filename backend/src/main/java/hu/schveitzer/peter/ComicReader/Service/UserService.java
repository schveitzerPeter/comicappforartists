package hu.schveitzer.peter.ComicReader.Service;

import hu.schveitzer.peter.ComicReader.Model.User;
import hu.schveitzer.peter.ComicReader.Repository.UserRepo;
import hu.schveitzer.peter.ComicReader.Util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    private JwtUtil jwtUtil;

    public User checkIfEmailExistInDatabase( String email){
        return userRepo.findByEmail(email);
    }

    public User saveUser( User user){
        return this.userRepo.save(user);
    }

    public User findByEmail(String email){
        return userRepo.findByEmail(email);
    }

    public User getUserFromJwtToken( String token) {
        User userToAuth = null;
        try{
            userToAuth = this.findByEmail(this.jwtUtil.extractUsername(token));
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(
                    userToAuth.getEmail(),
                    userToAuth.getPassword(),
                    new ArrayList<>()
            );
            if(this.jwtUtil.validateToken(token, userDetails)){
                return userToAuth;
            }
        }catch(Exception e){
            return null;
        }
        return null;
    }

    public boolean comicBelongsToUser( Long comicId, String token) {
        return getUserFromJwtToken(token).getComics()
            .stream()
            .map(comic -> comic.getComicid())
            .collect(Collectors.toSet())
            .contains(comicId);
    }

    public boolean bookmarkBelongsToUser( Long bookmarkId, String token) {
        return getUserFromJwtToken(token).getBookmarks()
            .stream()
            .map(bookmark -> bookmark.getBookmarkid())
            .collect(Collectors.toSet())
            .contains(bookmarkId);
    }
}
