package hu.schveitzer.peter.ComicReader.DTO;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@ToString
@Data
public class LoginDTO {
    private String email;
    private String password;
}
