package hu.schveitzer.peter.ComicReader.Service;

import hu.schveitzer.peter.ComicReader.Model.Comic;
import hu.schveitzer.peter.ComicReader.Repository.ComicRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

@Service
public class ComicService {

    @Autowired
    ComicRepo comicRepo;

    public Comic findById(long id){
        return this.comicRepo.findById(id);
    }

    public Collection<Comic> findAll(){ return this.comicRepo.findAll();}

    public Page<Comic> searchComic(List<Long> tagIds, Date fromDate, Date toDate, Long fromViewCount, Long toViewCount, Long pageCountFrom, Long pageCountTo, List<String> status, Pageable pageable){
        return this.comicRepo.searchComic( tagIds,  fromDate, toDate, fromViewCount, toViewCount, pageCountFrom, pageCountTo, status, pageable);
    }

    public List<Comic> mostViewedComics(){
        return this.comicRepo.mostViewedComics();
    }

    public List<Comic> mostRecentComics(){
        return this.comicRepo.mostRecentComics();
    }

    public List<Comic> lastUpdatedComics(){
        return this.comicRepo.lastUpdatedComics();
    }

    public Page<Comic> pageable(Pageable pageable){
        return this.comicRepo.pageAble(pageable);
    }

    public Comic saveComic(Comic comic){return this.comicRepo.save(comic);}

    public void deleteComic(Long comicId) { this.comicRepo.deleteById(comicId);
    }
}
