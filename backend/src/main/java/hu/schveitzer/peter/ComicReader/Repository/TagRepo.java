package hu.schveitzer.peter.ComicReader.Repository;

import hu.schveitzer.peter.ComicReader.Model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface TagRepo extends JpaRepository<Tag,Long> {
    Set<Tag> findTagByTagnameContaining(String substring);
    Optional<Tag> findById(Long id);
    Tag save(Tag tag);
}
