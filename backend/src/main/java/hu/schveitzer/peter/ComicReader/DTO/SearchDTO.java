package hu.schveitzer.peter.ComicReader.DTO;


import hu.schveitzer.peter.ComicReader.Model.Tag;
import lombok.*;

import java.sql.Date;
import java.util.List;


@AllArgsConstructor
@Getter
@Setter
@ToString
@Data
public class SearchDTO {
    private List<Tag> includedTags;
    private Date fromDate;
    private Date toDate;
    private Long viewCountFrom;
    private Long viewCountTo;
    private Long pageCountFrom;
    private Long pageCountTo;
    private List<String> status;
    private String orderBy;


}
