package hu.schveitzer.peter.ComicReader.Repository;

import hu.schveitzer.peter.ComicReader.Model.Comic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ComicRepo extends JpaRepository<Comic, Long>, JpaSpecificationExecutor, PagingAndSortingRepository<Comic, Long> {
    Comic findById( long id);

    @Query(value = "SELECT c FROM Comic c ORDER BY c.view DESC ")
    List<Comic> mostViewedComics();
    @Query(value = "SELECT c FROM Comic c ORDER BY c.createdat DESC ")
    List<Comic> mostRecentComics();
    @Query(value = "SELECT c FROM Comic c ORDER BY c.lastupdated DESC ")
    List<Comic> lastUpdatedComics();


    @Query(value = "SELECT c FROM Comic c LEFT JOIN c.tags t WHERE " +
            " t.tagid IN (:tagIds) AND " +
            " c.createdat BETWEEN :fromDate AND :toDate AND" +
            " c.view BETWEEN :viewCountFrom AND :viewCountTo AND" +
            " c.pagecount BETWEEN :pageCountFrom AND :pageCountTo AND"+
            " c.status IN (:status)")
    Page<Comic> searchComic(List<Long> tagIds, Date fromDate, Date toDate, Long viewCountFrom, Long viewCountTo, Long pageCountFrom, Long pageCountTo, List<String> status, Pageable  pageable);

    @Query(value = "select c from Comic c ")
    Page<Comic> pageAble( Pageable pageable );

    Comic save(Comic comic);

    @Override
    void deleteById(Long aLong);
}
