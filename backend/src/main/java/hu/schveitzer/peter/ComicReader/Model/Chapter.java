package hu.schveitzer.peter.ComicReader.Model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "chapter")
public class Chapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long chapterid;

    @ManyToOne
    @JoinColumn(name="comicid", nullable=false)
    private Comic comic;

    @OneToMany(mappedBy="chapter", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Page> page=new ArrayList<>();

    @Column(columnDefinition = "varchar(255) default 'Chapter'")
    private String name;

    private Integer chapterindex;

    public void setPage(List<Page> page) {
        this.page.clear();
        this.page.addAll(page);
    }
}
