package hu.schveitzer.peter.ComicReader.Controller;

import hu.schveitzer.peter.ComicReader.DTO.ComicDTO;
import hu.schveitzer.peter.ComicReader.DTO.SearchDTO;
import hu.schveitzer.peter.ComicReader.Model.*;
import hu.schveitzer.peter.ComicReader.Service.ChapterService;
import hu.schveitzer.peter.ComicReader.Service.ComicService;
import hu.schveitzer.peter.ComicReader.Service.UserService;
import hu.schveitzer.peter.ComicReader.Util.JwtUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Paper;
import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping(path = "api/v1/comic/")
@CrossOrigin(origins = "http://localhost:4200", methods = {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE})
public class ComicController {
    private final String NO_COVER_ART = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlgAAAOECAYAAAB97U88AAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOxAAADsQBlSsOGwAATDBJREFUeF7t3T9W29j7B+Cb31pgipysQKwApplq2m9nSmjSTZkujSmhS5sqTcwK4hXkTDFmL/5JxoAxsvXvtS3h5znnPUM8YMuy7r0fXcnSh5TSPC8AAIL83/K/AAAEEbAAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDABCwAgmIAFABBMwAIACCZgAQAEE7AAAIIJWAAAwQQsAIBgAhYAQDAB6xCyURpPZmk+nz/WZLT8HwOxvvyzSRqPsuX/BN4l7R4am6t9VDYfjSfz2SzvmtZNRiW/39PKxvOyt1CYjbPyv1FKDbu0e6XaVOmDKqiybDSflKaqFYMJWNl8vPWtzObjrOzvlFLDLe1eqZZV+mB4Zdtb6ABN5qOS97mobDlbtfzNSkMJWKPJcoG3GNJsnFKqurR7pVqVc7ACZaNxmszmaf7rV7q9Ok8ny8ffi+zj6fKnLU4/JmdlwPuh3UM7AlaUbJy+3f6ZTmc36fLyLJ2dneX/vUkPy/99NE7+SJ+WPwJHQruHN/YWsD79sWU+5+Eh3d9cpss8lBTB5MOHDzXrMt0vn2Kz+3RZ+rcbarEM+bLc3BeLVd/0Op1+OE2nF9fp7m6aptNp/t/8sbMjC1kP/6Xfyx+BI6HdwxsHnsHKg9VlHqhOT9PF9V26y0NJEUwOarEM+bJcX6TT0w/p7PIm3ZcmpNP0sc6ceB68vlanwEGY/jtb/rTF7N904E8QCKTdQzsHDFgP6eYsD1Z3/W6W07vrdHF6lm46TEP9/u+dzGHdfalYD/ln+uVu+TPwLmj30MqBAtZjuLoezC7PNF13CFm19gAHoVgPmw/LPtz8b0CfKVCPdg9t7ClgZWn1iyjDbJB5J/O/1fOpTtIfR3lW5126OLtMN6vHTR/u083lWTrVy8I7pd1DG6XXb4it1QvVbbl+VOMa5c9WJfL1Xl/PazIq/503VXUdGdeQUUoppd5V7f0Q4cPNl3xfaLim33+uzGLV9Pu/5n8DAAzWngLWp/R4lYaH9PP7wKeTp9/Tz2VaOq31NUIA4NjseQZrlv4d/OH6aXo356wDADux34D1Ti5Gd/fjnVzYCgDYif0ErOxjWnyJ0MXoAIAjsJ+AtbiNzIf04eKdXIzu7mJxWx1fTwYAyuz5HCwAgPdPwAIACCZgAQAEE7AAAIIJWAAAwQSsQcnSaDxOk9kszebzNH+u/N+zSRqPR/lv7FOWstEkf+2n5Zik0fL/hMqK9z0ped95FY9NKt57Nl783Wz8Dq68n41e1sXzel+tx21hkm8no3y9HdTq51a2rIvHZ2kyyZd1dKhlfVzG5+1qNq7fhvLPYlz8bf4eXr+35TYZ9J6y0TiNJ2Xr8PGzLl5nv2tvT+1+kyG1AY5e6U0Kh1H7v9lzq8rG85dbRJeoutlzNprnHezyl6vM5pPxaJ53K+XPFVBZvjx557Z8vVXB63rxOsunrq14/+N53rEun+flRuN5wHr9/EOp/L2M8hXReFUs5H+Xb1/5OFP+3OGVL+t40nJZ86Wd5dvQ82e3u8pDS/m2NRtXtp2Nf1sq3x5Hbd5P0/VYbPe7XW97a/dlNag2oNRzlT44kHr/ASvfU2zXqdQYKBpV0cFVdvhx6zp7SkWBhhewHgeVGLsO3pHLmlsErbLX6VDLHZWtS7mt3TTa0VnToD22bvOFgbf7tzWkNqDUmyp9cCD1ngNWQMcS0Nk221uPWNfF+14+3ap8z3nROa4PuvkAUCxjnYFvSAFr0yBbzPCMR/l7LlkPtWY8ogfgvDYHgmLmYHU2cVnLz6zOdrWYeVj928ZVvFa+fDVea2HT+qlqw3VUrvugMDHIdv+2htQGlNpQpQ8OpN5rwHo5rNVVq1DRek+9+7ouC1ezunudFcs9jIC1YZCtPaNTY9spnqv0b5vXpkBQ9zOrNZC3WN7Nh7MqlA2+GwNkc5u3wbg2Xxhau39dw2oDSm2p0gcHUu8xYMV2tM3f/+PrzybFXuJoPlpU3b3Zjuu6JF21GSg2HV7sf8Da8Nk3nsWp0S4678UXg+DyuV6Zzce1BsHVqrPNN9m2nrbhcfNteH29lL/JDsrWT3SbLzT9HA7Y7l/VkNqAUpVV+uBA6r0FrJLOJd/TGpccGmuyhx4TLOoMAl3Wdcln2aEDLAtZ/Q5YmweW8t/fXrXOYWv53EVtyh2TUfnvV9eut6+iGg66b97kyyHPV9vl4pBng8OQr9Z7+ftevE5xGGz1dfLfbTQz1+Hzfal9fC5PNaw2oFSNKn1wIPWeAtZa51JzOnzz+S8rovbUKvfm26/rss6w/WBd1NvOus8Bq3zVdth2a50z1Ga2acvA1XmwahiAWlTloPv0/K/aVd2To+ueQ/Xyub7+3OufhF0cWt3V5/umdtjuV2tIbUCpmlX64EDq/QSs1c6lcRCo7ACDOpHKDqvtui7fc+0WsPJaWy99DVibBv1u779O22ixTjZuA0HtrMag2OlzrGorRcDKf+dpGdqcZF/ZHHPFpRte/V7+uo2/NVnjhTq3oaJ21u5falBtQKn6VfrgQOqdBKxn+R5sqw6leho/pKOtXN8t1/WG9dN9mV+vl152pJu2jc6zjtXbxEKjWafNzxm5bqtzQ4cdhkZtse17qjewP+my7irXVedZxaJ21O6falBtQKn65UruvfGQbs5O08Xd8p+NTNP3nw/Ln8udfsy7qr769Ec6Wf64qvsyT9P11/vlz/00+ueq9L3ff73Ol76L6m2isdE/6apsYfNt9+f3bku76u7iMm3/1E7S1bcGV1xv7D5dLtpi2/d0l77c1Fn3D+n+8iydXrdfd3c/Krbv0497vsp7c4NqA9CAgNULj+GqQz+bpt9/5s+y2ckfn5Y/dfE7/bfH/urkz7+7Dw53PyoG6wMaTdLt+fLnV+7Tj1ZB+7WqbaLw8N/v5U9VRmlSvrD5k/xMgfkqd5eqckM6uUr/jJY/NzH9N82WP5bLw9WHi9Q6Wy1Vr/unHaqOL/T7v8rPuLsdtvtBtQFoRsDqg/uvncJVLQPYk30jH0S/jbsudY3B+kBGf20ILPc/8qUOML1Op5fb3vx9+lp3wxv9lTYsbUqzfzvONLx19+WmcmA8/7yDWayH//I4EaAqyEWF0qrXOfkjRexa7cqg2gA0JGC9F5V75sN0cvUtTdrMVKyoPIxyCNk4fd40IRS5R313kT6c3aT7V2nlIT3cX6azYqZm+ch2WRpvWtjcfcRUw7rp91R5dOfkz/R3b/caKmZ9woLPfmeVQw2qDUBzAhaHt/Uwx0k6v52nWZeZrL0cRmkm+/vP0vNOCrN/g/eo8734i9MP6cOHpzpNpxd39Wedsr/Tn5sWdmfqnD9zkq5aHSekDwbVBqAFAevd2Nce8w7UmH07ufqV5rNJGrXJWcVhgrxT7XIycaws/b0xsTykvp0Ssm0g3OXy1jl/ZpCHvkNN079bG89p6uf3W4bVBqANAYseqHme1Ml5uv01T7PJuF3Q6o1P6Y+NiWWWonfeu9k2EBZ2uLx1Dnv3+jAhmw2pDUA7Aha90OQ8qZPzq8eg1XZG69Cyj+l0+WPvHeTw4JM6wfskhXxBlv0aUhuAlgQs+uHuS6p16aAVJ8sZrcdDhwNKWhuu+9VLB17W3zXO4O71Nd4oN6Q2AC0JWPTENF2fVl1gcoNF0Ho8R2s8gCmt7ONw9t0PvazT7ScYLcRc4419GlIbgLYELHrkLl2c5SGr4UzWszxoXd0OJ2iV69dJyZ82nygDO9LXE/OhGQGLfpnmIev0LN20Tlm5ZdDq68nwQksDPbzEBt1pAxwDAYsemqbri9N0dnnfaXB9PBl+liaDms0a2knbZhuI5osLvA8CFr01vbtIpx/O0uVNl6BVXKi0mM0azgUpnbS94p3eoYDttAHeAwGLnpumu+s8aJ3lQavDYcOT89vehKyqb8aF3OR6bw4/2+BmvcPzvtoAlBOwGIZpHrQuTtOHDkFrEbI63zx6D3p+g951h55tCL+tCoc3sDYAZQQshqVj0CpuHn3ojFV96YHz9NeeJttGk9liZm/TKqlzHarDXibBbVWGaEhtANoSsBim56B12fAbh8O4QfD55/EeOvxR+uv8JJ2cflz++60616E67P0A3VblvepLG4C2BCyGbXq3/MbhTf3rZ51/PuwsVp1LD+zhHnvZ+HM6L36Y/Zs2ZpRay7rDwzlVt1S5/5Hulj8yIENqA9CSgMW7ML27Xlw/q95hwwOfmD39nn5Wjy47nmkbpX+uHq9FdP9jS0Sptaz7O5yzbuuy019DagPQkoDF4WXjNJvPU/cv+T0eNjyrcVPD80MlgoVpqnPkLZ3fBqyTcs977uk+bR9bpul79Ui4u/W59Z51VctOfw2pDUA7Aha9ETVIT69P02Wrmxruz92Pegt4fjvJ97OD5YH223LPvc4htun3n9WHc87/il/O3NZ71jk8OGhDagPQhoBFfwQO0ncXLW8cvS93P2ou33m6Dd2Fz9L429XzrFCtQyMHPEy47ZYqDusM3JDaALQgYNEjkYP0XfpS41Dh4dylmjvw+WqJu0jqaPItPe2450NLzUMj03T9tXph47/1laWNE1gPN+mLcXHghtQGoDkBi16JHKS3XWKgD1f/vvtyU33obenpSvTt1022uN7P7fnLjNDDzZf6h0buvqTKvHpylULPSc7+Tn9umMC6/3qdxz6GblBtABo6goDlZrSDEjlIb/kqeC+u/j29TjUmhp4VA8yv2SSNG928OkvZaJJm81+vBpZiz/3rdZN1ME3X/6seDCMDcvb3n+UnuN9fpguj4vswqDYAzZjBIlBMmA07qXXjN9D6c1ig8bliJ+fp6vZXms9naTIepVGWDx7L//WseGw0TuN8b32eDyq/bs/frIdWe+75YPi/qmmsPCB/C7nIWJb+Lp2+uk+X0lXPdGv3g2oD0NB8sJWN57N5tcmo5G/3WaPJckk2mIzK/65RZfPx1pUxm+fjXsnfNakdvUbZ55ivk7zTLP/9mpVtWNjZOCv9/YNV1fYRbTbusG6rtoFC921t02fXvS2P5lvXdqd1s1pV62kyz3ciSv6uaQ243a/WoNqAUvVq2DNYW6+R8+LQN6OtdNBbjTTxKW35Ulcu8AKexaGATudbvFxE8LUeHha4u6h17a4Y9+nytMv5S9N0fVo143CSrn51mIVc/Qr9ioebs+6HBquuDL+3mwzv69SFiDa5h3Y/qDYA9ZUmryFU7Z2ekBmi9lW9nAF7szVm8zrP3OzqNbY97yxfNy32jjet84PPZm6p0aRq7XYVMZuxrDqzx20+uw3PGzbrWNkYg9ZRjfUTsy1WzMjletvuS2pQbUCp6ip9sP9Vp4N/dshGVd0BFrp2tpsOqbzScVq8VqBt8xqVn+VsPhnXPWSYbeykJ6OgQXqHVetzbGUHbSDLt+3Kxc0/u5rrPcs3sLKnCwtXtQ5vxrxerc8xYMdv0O1+Qw2qDSi1vUof7HfV6tjXtJwJ6VbFYL98/Uodlq9B2GwdMuq/keYDVIPln03G83H+HrL1dZXl63pcPkAPrWPdFDRa2+n5JnloqdMYZ8uQvOlzK3uKvM1Gfm7Feq2n4/bSZHvuEub28Tq7bPdbalhtQKmNVfpgLyvLg1XRmXdpeMUAPco79Z02tm2Dxlb5e2uyfFtDxWZNXiPbw2s0GSiaelyOktfse7XZiXijCDVxg962ih0Qm8xYVlURxh/7jWaeAmHdvqLt6+SvVATJsp2GDdX2dfLG8Pg6Jc+5Xntp91U1sDagVEmVPnj42uGgW6b7+RD1Dj809XqvcDevsf7edzFFv3Xv9vmzXpnFe+rg2y7KYsZy+B1rNhq3GGQiA0qTygfWImi1/szy5a4ZACqrwcxLbW9mQXbTHhft4NXr1DxM19B6m9x7u69Zw2oDSr2q0geV2l8tA9a2zrjYay8C1yTvaWdlI3j+2GIm4N12qkXgLAaa4n0u3/OKYp1MlrMH5X+/78qXtxgYN31eueLx2aSYVTEQqjo1tDagjr0+LH8AACCIK7kDAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIdmQBK0vZaJJms3maz4uapNHy/wAchWyUxpPZsg/Ma6IXhF04joD13KH8Sr9uz9PJyfJxgKOQpdF4uXP56zZdnesEYdfeccBadijFHpoOBThCWb5zOZk97lzeXtm5hH16dwHrTYeyfBzgKGQvO5e/8p3Lc6kKDuJ9BCwdCnDkstE437ksZuztXEIfvIuAlf39Ld3+mdLPm8t0eXaWzs4u0839w/L/Arxz2Th9yzvB09lNurws+sCz/L83SS8IhzV/rzWazCtM5qOSv1NKqXdR2Xg+W/Z2G01G5X+rlOpU7/pbhHdf7MEBR2x6nb7eL38G9updB6w0/TfNlj8CHKPf/9nNhEN43wEr/U76FuCYTf+1mwmH8M4DFkCkUZq4+jlQg4AFUNfor3S+/BFgm3cesKbJ7DgQI0vjzwOMV7//82UfOAAzWAB1jP5JV67eCdQkYAFUGujsFXAwAhZAFbNXQEMCFsBWZq+A5gQsgC2y8TezV0BjAhbAJsVNlKUroAUBC6DUKE1+XSXxCmhDwAJ4Iw9Xs1sXFQVaE7AAXlmGK1NXQAcCFsCTTLgCYgwjYOWd3mg8yTu+WZrN5mle3Gx1pWb545PxOI2y5e/3TZZtXf754vH8PUzy99C7N5Hlq3+cL9tksYyz9WXPa/F4/v/H+bLvbelX1+n6Mj0tz3i0eXmy8eLvZuO+bjRr8jYwnry81/rLvfz83qyn/N+zfP1tW0f70pP2kY3ybfyXcLVZ8Tlt3pa2trddKbadRf/0uI286Quely+vpz5hIE2+v3o6JvTUvJ+VzfNOd55/eM3MJvP8M31+ntFk+Xip/HdfvWZktVz+pbzDyt9HVvK8+6m8Ac0nLRd+p8uejVos12yeB/CVZcrm4+Vz5EHl9fP3qrL8c8i3oZL3W73c+fbXYEXNJqN53hGWPM+uqkftI9+m8vC6fOZg+Xotfc19Vjbevp6rlrHR+ina2q63pWW7WL5iO12WM++Dls8SouE2sn1M22Q2z/fJSp+vbvV2TOh3lT540Mo6dLxPngag/QesZgNbpbXAuOvq0ojWzSZFqCl/nTaVPaWiQH0MWNkiRG5/r9uWu/XgU2xrJc8XW31qH8HLUmbgAav9tjTeScjqHqzW5UFr1KYPKELeuGUwz8NOq9d8W499xfJpSzz2wd1eq89jwgCq9MHD1LaNJR9wFnscax9OsYFt2siLvfJ9BqzNjX9WvqHn/6678e5+hiErXY9Py/36tZ+Wu8aCLzqw1b9tU8VAuHy6VRu2iaf1Wqfz60/AajajU77cAYFhRwNjUf1rH8EzEWUGG7D6ti1tW55t20++zdXZfrr0A1Xr95XuM0lvq3w77t639XlMGEyVPrj/2riR1t3DaNMhxAWsTa89qzkNXWsg2dUMQ0mwfWxEJb+7XhV7UE/a7SU+Vlm4qrteqw5vHDpg1Q0Q694u98thz852EAoG1z66HlbrUzV+L3HbUkz72rY89WYwi6BVpdOgXzdk7WQHpmT9dH2dno8JA6rSB/dbW8JV07RfpyG9iOiQi2C3fLpX2uyp1OnYgkNWybpvvuFvWgevterASp64Tae96fDiQQPWYt0v975HxUzcqPYs1uvlrteh1Re5lz3Q9tE4lPS4Gr2XuHD1qPvnsblvafjclZ1Ux2Wt0wnuJGDl7X/59I86tt++jwnDqtIH91cbG3++sbfcSOqfq7O7xt9+w9lnyFpvmN02+OoG1bThv12+Lh1U2XZx0IC1qaoGxNzLcpeto6dDp2vvLf937cOQQQFisO3jKANWybqdTebjksPwdc4TfNKpjW3pVNo8b1Uf1a0/qLFt7iJgrX++nbbNvo8Jg6vSB/dUJYPDQveVXv3BFrp1xBuDXOfOd9N6WdG5ob7tDLqHjdjlLlu/3fZ4dvGed1P1BoK191MMhnX2NIvBcfknm3UPKYNuH0cXsN5uS3UPvW197kLrz2Lb59xyjKhuWN22m8qBJ2rn+KXW21nkzkvfxoQBVumDe6lN22LIoFfVqSx02Ng3Pn9QA6qx/F3W09t1H7PcGwfVFfU6gLeNvdAtYOW19sb7GrCq1mNx7tLqr9Q+J+2pKgeCjut64O2j8vnfWcBa3Rwar7fKbWkXYajtdlQ14HfdPqsDRec+7FWtv1775X+7uvs2JgyySh/cfW1sPEEdcF6V7b71a5UP/oXIAbt6+Vt2XCUdbthy1xj48herDgMbnqd7Q3z92fU1YNUJQI/azvZu3oaftQ4RA28fRR1TwHrW9ttd1dtSm+fd/vnG992POmwzy6rcLiNnbNZerHX7GsKYMMwqfXDHtTnlhw54lVt6y0a68Xm7N87XtYvp1bIOJi7U1lrmOutpwzoO2T5Wnjt0e4usym23kH9uXba3qtdoGyIG3T6WdXQBq9tnUz3j2rSd7S4Ibd/sA7bRyvUd1w5ev5e2zzuQMWGAdZBb5WTjzxvuUv+Qfn6fLn8O8Pu//BmjjdLkdsM99h9+psjFT+ku/bhf/rjJyVX6J28JtWV/pz/XbwVy/yN/pSi/03+VK/0k/fl3Puy1cPLn391vvXD3I1Wt1v67T5dnF+muy/ZW1T5OP7ZY1wNvH0fpId2cnabrDp/N9PvPrdvSyR+flj8d3u+tHdRJ6ryo0+/p59aGdZKuIjbKbJw+rza1+6/tPsOBjwl9tv+AlW8U36423OwrugOu3NBbGP21IRzmZv+m0PEjd/flpjIknn8e1x4IR/9c5Zvyaw///V7+FGGa/p0tf9yidVDKB8xv+a5ONzUG5kOrCD8PN1+6havC9N+09aM6+SM1HmsG3j6OUtuBuYlWYX2bWfp318vc2jRdf63oYM7/yndFusn+/vNVX37/o10kGvyY0GN7D1jrG8Wqh5/fwzvgWFkav9pleK3tBr5VnZB48meqF/5H6a+SxZ8F91Tb9xCX2gzeSydX39KkY+901/uEtQ919iybGHr7oLWqsN7YNH3f9sE+/Jdvve1M64z2XVXOkp+nz512FEfpn1cTFfepXfN6H2NCX+05YK1vFK9Ff6hFIw1tS2VTqTtX0dEs1Jxy3jC7cH779m7oXerXls+4tq0zOCeLZZ516aB2cvj4yA29fdAr0+uvG0PK/dfrnu+M36UvN9u3y04zNmt9+WJGe/lzI0MaEwZovwFr2+GDfLgLnZXcgW2zb7tc/qrzGxZqTMFnH0+XP/XBafq4bYFr7BGfXP1K89kkjdr0UtPrdPrhQzrd+bGRPovdARl6+6CLitnQVrMTd+niw1m6uV954of7dHP2IV3sYDI0WuV22eH8wNGraaf25y4PakwYoL0GrNcbxbo+H1MvZOnvrbvnO1z+OtPvlYdBqpa/b2qeJ3Vynm5/zdNsMm4XtAgy9PZBP03T9cVp+pDvDC3q9KL9+WLZKI0nszTb9CWMaPlOXPWpWC0SVtTJ7YMbE4ZnjwGr/Fjvsw7H1PfiIIc/ntQJG1XffvmU/tiw/PeXy85rr1X9raUm50mdnF89Bq22M1p0M/j2wfuUpdF4kmbFoapft+nq/GTLLGu8yj6sxcnuUSe3D3FMGJr9BazsY+rTZGRjn/7Ya8NcV+ckwdNt86tDXP93X1LFaQxvnCxntB4PHUpaezP09sG7kmWjNJnN0nz+K91enR9u26zsw5qe7L426/Rwk760zVdDH5MHYH8B68AdcFeHPlZd55svfbrWTIxpuj69bHfNqkXQejxHa2xKa+e0Dw7vZbbq16/bdH6yNuIU529dnqWzy1Y9SkvVX8JodLL76J+0er54/795f9z2FrD6dTJdc582zaUOxZaA2+89+7t0cZaHrIYzWc/yoHV1K2jt2uDbB8O1PLeqfLbqIc9Vl+ns7MPj+Vt3070HksiT3aNObl8Y7JgwHHsLWDrgjnZ4WYHe79lP85B1uvZtoqaWQcvJ8O+Uy24cn6fDgMtzq15ZzlYV5/WcXtyl6SGnecJOdl87j3mHF4g12xtjTwErSwOfwKphwF8xHcRX2B+/TVRM73cZSB9Php+liZS1Z+/vK9gcyNOMVclhwIc8WF2uzFb1RfXJ7p9T1alY67eY28mFe5+4rEmI/Z2DRTfhV0peMaAr6E7vLtLph7N0edMlaBUXKi1ms1pehIb+2WX7oCeKc6y2z1id5sGqR7nqReXJ7lX34gs8ub2Od3hV9UPoT8Aa/Ad6+K+Bt79/1NBmF6bp7joPWmd50Opw2PDk/FbI2pshtw8ObnE4sDjH6u2pJotzrHo2Y/VWjZPdr/7ZfMmGvZ/cbsY5Qo9msIb/gR76xMD2txoa6J3Mp3nQKi5C2CFoLUJW55tHU8dw2wcHNZqk2eJw4PLfK+6LWaviHKvlv/ts261/Hp2nTadihZ7cXstAx4Se2VPAqnNLjn5fCLDOdXYOe2Jgxa1IKk4CHvSdzDsGreLm0TJWN4NvH/RTHq7mt+XXsSrC1UWvZ63WVV8Q9/zzuKQf3tHJ7e95TOiJXp2D1eevhta6A/tBTwysuBVJ1TkqHe6L1VixRzoexa+r56B12fAbh24G3NXg2wf9k40339bm/nJg4erR3ZebraGm7JZOOzu5/RjGhAPbW8CqtYfb58Rc52vguzyPrOqqu/c/8v2jbsr3nqJlafw53yO9+ry7e8NN75bfOLypf/2sGt/iYYsjaB/s0yhNfl2VzlwVs5E3Oz3De4em39P2U7HWd/b2fHL7mnczJhzI3gJWrT3cPt+QtbJhFDYfQ9+16r2aGvdr28cey/M963Y/ozC9u15cP6veYUP3qutk8O2DPlmftXnl4Wfa+SlIOzNN19UXxXo52X3tHp+xJ7cf35iwb/s7RFjrQoD7PlTT5MT66m+BFFrdHb2Orbcauk91xo86s4jnt5PN32QJMPpnuVdaNaNQHB6Yz1P3L/k9HjY8q3FTw519dkdh+O2Dvhilf0q+Lfhk8LeHuftRebL70/0Jn/vLhfv0NfjKooMaEwZofwGr1h5ursXdxfel8pYHhR0t/9ZbDdXcMGstf964b2c7mhYeTdLTKRV1ZxSiBuTp9Wna6y3IjtDQ2wc98e5vQnyXvlTs8D2eLrN+cnv8djzEMWFI9niSe7093MWH2ddrEx3wMMi2Ww3V3jDrhtyTq/QrvEGN0uT5hNUGMwqBA/LdRcsbR1PP0NsH/bB1NvJ9qHV/wslfeWt5sZPteIhjwoDsMWDV2KienN8GHBrahRrHz3PxJwZuudVQo5Me64bcXGiDytJ4dvvSWTTaE4sckKv3HOli6O2DPtg6G5l7F/fJq3N/wvOVeLWz7XiIY8Jw7DVg1dmonpzfzgK+1bWDeyBW3vIgV+x9RAbEtRMdV91/vc6bSH3VF7tbsWhQk46fQ9GQfq1chbj5N4AiB+RtX7Zwpe8AA28ftblX2+F0nNWuCnD7Unl/whW7PO9siGPCkMz3Wtl4PpvXNZvnH2T581RWNh9Nlk+zxWRU9rcVVec9zMbzfBss//uGlY03vNpkVPr7VbXx+baYTcbzUaPPIptn+Qfw5pXqLvPaOm71OZXVls8u7DUiqmIbm42z8r9rWNvbyGSeD2Slf7e1Bt4+HmuUv/ttWq6bQ1TV59FpPT1VNt/erdRfX3X6p3bbfzEm7Oq521TVOnuy+21tEGPCMKv0wZ1WneDzYpav/6YbfN0Nt/hsy/6+unbXCazXpvfSpdHVXz9vzCbz8Xg0z7KS95Y/lo3G8/HGTqxBYH4zKAR1Mhs3vp4NmFWNJKhT2v4y7Xdwht0+iqoKWMVHUPZ3q/X0HAfetgYWsCq3/YVm22bRL9XIVgsbt8tstOj7Sv9fy6oVbPYSQAYwJgyzSh/ccVV3Xutm+UZWZ4+3SUNaaL3x1tkgu288mxpg59mWOrMMwRoNqGXLV3Mb2Fab1uf+9lpr1sAD1uDbR53l3zoL9/L3B9+29rItVa2vBp917b6pxnMWoWhtQJjNKp695HMtxpXFXwW1u5eKCPJB1fcxYZhV+uDuq+WHuZiWHOWp+NXzPabkN8EqbwzVO0Nd9i7rBMUOz79hHYVtlLX2FIM07Zg2bR/587QPWZs+rwPPMJRU5Z7tXgbF4mXK/q5uDbt91Goe+d77q8Mk+R77aLx6GKQH21bVGwk5XBsYsGpsl6veHqraMB7ky7A4GlLjg305avL6sOIuws7WxQk8lF6r+jwmDLNKH9xP5R9mg3bUzHLDrLW9FFOcRWhbTGc+7vHMio5zfXnLqk5QXO+E69Suw9WyimPiO9emk9i2Xtusz7w2vdW97SE2qMqPJaTjrQ5Anbe3IbePgLYR3V7bVPXbCAiBNT7nRusiul9a3cZaPndxFOXNckbUluU5xPbT2zFhmFX64P4q/zArO+CmVj68NtvKIlyVHU/eVFk+UFWPIit7Rdur9ETA3K4a26bXC9G2IVV22Pn6LI77l/3tm9p8cmvz8/v2UXVmfgKCYZ3GEdERDrZ91PscNurFILKfbanWuUSN1kezWayt1l+3Tuhft9PZlk3vNSD4tqxejgnDrNIH91ttNvhN1j68OmPIwqzJgF1WeSOpHkVeXmd9j/3p0ELZU+SBr+u5KpWVfwZ1Fr+RLg2pwTZRHCJ4nIFcf471wzWrup//s6uqNVgtdOiAi9CzfJYqMcFlmO2j/mexpheDSLFjsVyeSiszPE2rQVtttkPTMeDmyrfdhuGt02kJ9ap0Ozv0IbS+jQnDrNIHD1CbZxnqKmtMVR1M86+abq/Y5N819DWt7p/Bo/qzERsrMnSvif7MoyorOSG32lMgqbm+t4bOzR7XWR5iy56zQQ2xfdQPKUuHHkS2hdGtZs0+531sSw12BF7bvgNVNzjv7LDgm1p/n33ZAezRmDDMKn3wcLU4nNDsA308pFfyXHmVdo7FXnIx41Hy+zGVP3cxkLTdLne+fBVVdJytGlXRQQcNeM8Ba+WzbT1wLDU99LuHaj1DssX6jkbjgFBHp4FneO0jqxUkDjmIBB5SW/F6W9rNa1QfnmzWH9XrgyreSzEruufP8lU77dtMTx/GhGFW6YM9qGIwHc8n+Yf6tiN+PAl9ce2N0r99qZeN9mnvrPz3dlf5+yi+0bJ4H+UbaPH4bFI06P5tiI+zKsVgmC/jcnlfPL6nyXKPtOzvW9cyYG07PFUsWxG4Nq7bYplrbifqUDWk9lH0SevBcLmNHXKH6FjqeQdrfTtp286Xz7d8lsJBP8uVWftt/d6h62BjwgDrw/IHAACC7PdehAAAR0DAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABAAQTsAAAgglYAADBBCwAgGACFgBAMAELACCYgAUAEEzAAgAIJmABQFvZKE1mszSfzx8r/3kyypb/k2MmYAEV8gHkafDYeU3yVwswmpQ8d0DNxull6NznetldTRqs8Gy8EiTeRXXb3rJiO/t1m85PTpaP5PKfz29/pXm+YsWs4yZgARXu0sWHD+nDh7N0dnmfHpaPRni4v0mXZ2f5cxfPX9RF/moB7i6Wz5cv89llurlvu9QP6f7mMp09Ld/pdZou/8/Lelmp/L1c3uTrKHIl0U/ZOH27PV/+o8T5bfo2FrGO3VwppepXNh9NZvNuJvNRVvbcu6tsNFm+dh2z+WSUlT5PvcrX0bjrOtqPyahs+csrG8h7qi/fDkveZ52qtzm1f371Lqr0QaWU2l7ZaN4uZ83m+Y59+XPuuGoFhNk4KPxl83p5ZDfrI8uyvEZ50Btv/Zxm4/pBcmuomOWhdJy/3uJ1m4TTfDtaPsVmDYPK83ufFIu1RdsAVGeZHzUJsOrdVemDSilVo+qGiBV5gMlKn2sPlY3zOLPFZBS7bLWmOfYTOLNR+XvvHrC6zvbtIGCtVfHey0Nmy3VftR2taLJ+1fsq52ABHUzT9f9uQs/L2qnpv2m2/PGNh5t0dnG3co5VgN//9WbdTO+u0+nZZbpf/jvGQ7o5O00Xd6FrLVzx3i9Oz9KNc+PYIwEL6GZ6nf7XZOQ6+SN9Wv7YH/fp8tUJ7EG2BbpDmN6li7OoQPwYrq77na1W5DsDQhZ7JGABnU2v/9dg4DpPf42WP+5b9jGdLn98UQSFoG8vDkEeiL+uTGOd/FE37mbp48rKe7j534DC1ZP1GdeTVPvtQ0MCFhCg2aHC8wMlrOzvP/Mh9bXdBoXf6b8ezpjc/eh6oPA+fR1eunrUdMa1TIOZydm/A11PdCZgATGaDFznn9P+LxE0Sv9crcerAQeFLu5+dDoX6+Hmy6Bn/Kbff3Y8TNrP4Ey/CFhAmOn115oD90m6+me/s1jZ+HNavyzk/eURHRp85S49T2Kdfqx5xfFP6Y9FPn1IP78PPJROv6efy4B0+nGXSf8h/fd7+SNHR8ACAt2li8uacyPnt41u09JNyezV/WW6OM50tdD+MOEsDf+o1zT92+nbB3X//j2sK9oSsIBYdxepfsYKuvdghbezVw/p5ssRp6tC20tIPPyX3sOkTNfz0H47RkgFAQsId3dR93pL5+nzrk/GKu4ZtzZ7NcxvwAVregmJp29gzv5NJmVqeidhlPZKr0CqlFKdqva9/3Z5JfOSK83v9Uryda50f7hbB/Wjdn8l951Une17Mir/W3UUZQYL2I3ahwpP0tW3cc0TrRsa/ZNeT149pJv/7eCCohyfGodYH5zhftQELGBn7r7UvDbWyVX6Fn6ocJQmt2vfG7z/6tAgsBcCFrA7Da6NdXL1LfTaWKPJ7dqJ7ffp8pi/NkisGuewucjocROwgJ2qfxudwGtjjSbpzeTV0V7zit2outioa2AdOwEL2LEGt9EJuTZW2aHB477mFbB/vQlY2WicJrN5ms+LqnltnGyUxuNJms1my797qln+2CRNxqOU7fgb4KXyFx0V72dSLEder5ZtdRnzmkzy93Cg5Vy3XJ+TYrmeP4uVWjxerNdxGvVigVdlj9tQvj43rfPn9T3Kf3f5VztXbAtP63R9mZ6Wp/j8l7/+RjZe/N1s15cy2LUGhwq7XhvLNa/WZWm8aM/7uebY8ai62KiLjFLy1cK9VTaaj/NU9db2r+Rm+d+V/tkms8k8H1RLnyuusnk2msybLNZbs3keCvf4FfKisvkoX5ntljv/u0m+vAf8inkeqpptCyvysDjPg2Lp83auptvoQvH5j1eW6eUr/nnAev38g6w6lyxYavv19mz8Zls+7Lrrw2Uanpahh5c6WNRAL9OQV7btw93r5UBUT6v0wR1WPqCPq4LIpsb0GAbaWoSB0uftVt2D1bp8oN1DICz/HJYh71WHX4TH7UFmV+t2U3UJVutmkyLUlL9Om9ra6bb0PgJWXiUBaJPJqOTvt1ZJmDn4INeHgPUUYASs8Np2LSwBS5U8sJNqNiCWNaYGe7/bFLMWb567bW0LfMXszupsxLLyfy8CWY33srNBddPsSo0OIS48FCGy/DW2V74dlCz807p+vfxPwbDOMrddntUqtofl063KX/9taM1rsS2MN8zivvZuAlZe28akVxoOUG+3zT5cwLMHAes51ApY4bVtY3aRUVXyQFxtPARYZb0xBYWrZxGNddsy5c9fo8MsglaV7oP+Wm2aQWgwmEWFrMahoSQY1p59qnnIrsvMYdnHOat7yLeirbyngFVvQH1U+3337tDgUx0+YL20VwErvLbMyL6vNqtaVumDIVW062IAHI9G81FRxSGpys6msNqYosPVUsfp283ZqGFHUBmyIjuWTR1Z89eokQ0rNJwxKunImoehDTNMa1qF2pInbtPBbgqv766zrr0B1Qsfb5+uLwPyoQPWapvvyzpZrwEHrC3LLmCpvEof3GHV6XBeGlPcIakSbadwtwwObRpV1VgT01A3r/d2z1/VKUYOGm9fq8vMXvXY3nTZS9ZFhwBfts2/x866fsaqWJclTxQ+89u6DhuwXq8aASu+Ni97f7ZBdcAqfXC3VdmzLhtTye89zohtOtdm+UsNNG8E2zqDlh1l1froONtW1OaXaN+5V4bfkHMQ3g5Q3cNGjQ694yHTbp3rLt5zH6vOwPpo8/sveY5enftyqIBVfIll/YUFrPja9PnuLjSrQVXpgzuuqgZVNKa138kHvHrnNW0+Jl6qaXjZGobadgJ11kfZ39WsLecJNH7/q7XteRe6dzJvV3dMR1tnZrReSCrvYDvvva698fcZsPKq2rl4Vr4t7Wr7iKv9Baxs8aWJx3P5yl+yb+vmqYYcsDbtvApYalGlD+64qgPFeKVXaj645M9fPX4+azIYbh8P2nYCVZ1wt8a6dZk77e1XDx6dgsEuT1yuDIe5OuFzw/N0Dlhr6/bdBqy8ames9W215A+7r/foqhOw9kXA2kWVb7/9XV61v+rprXLO09XVyeKnh5uzdNr49vd36eL0Mt0v/1Xl/PO45pW9s/TxdPljqdP0sd4Tram6InAXo/TX2l1DVj10ulnWNH3/uf3q3Cd//t3yqulZGn+7So9bwZP79LXxtrBBjRu15guf/q5a+E9/rC3jo9N2G8KKabr+WncLHra7L21uo5NvH5/XNuyHm3TUF2znIH6X3ZDw4b/kNoQcKGBV3STzUbtw9SQPWWc1O+46A+mOlTbSZyfpj0/LH5sa/bV225DXut7tfVqVDE/+SK0WPfs7/bmeXO5/BN6st842eJL+bLlhtA+WK+5+1N5JGLQWt9HJxt/Sch9s6SHd/O86j6WwX6V94Oxf2yL9vdlzt3C1VLvjbj+Qvta/e0+Ntk1fRfj9X0WIbTerN/pnffaq2CmM3CesN2vYOiidXKVv467b1F36cRyTWHlT/V+ql7HO0+1skr69Tld5f/G/FDW5uX95ODz7kD586FZnZ2fp7PIy3dzXC6vAbvU0YMUdCppef601C1BvIK04JNZhWrhyJqiVqkOaASoPtbWZfSs/rNl1tm3d9lnDpbYzcLmTq28rh7TauTuWhFUcEv1f3Rnn89fh++Em/W+46SrMdDpN07u7dH1xmgeus3QpaO1HyU5m7M4gQ9XbGaw4d+lLnV3jmocJtwW2+6/HeIii3uHeRjYc1jy/naf5PK5+rc2CtLJ1Bu9kscyzLjNZlTOE70iDQ4UvHBosN013edC6PJZ8fkglO5nRO4MM0xEErHz7//6zxiBVd6blLl3ke4evpuEf7hdT/BdOsA2R7XzarYmKQ5w1TpY/ufqV5rNJGrXJWXnoOP3wofvh8oGof6hw6f7rgA8N7t7dRf0v+wCxjiJg5QkrVXzZbaH+N7+my2n45fkPpxftO/lslMaTWZrd7vhcqQ26f9stWpb+fnN2e5/VPE/q5Dzd/pqn2WTcLmgdjQaHCvPocGmvpsLxnMd3OOuz+A/JEUIKxxGw8k676nIC+5Wl0XiSZsWhql+36er85M0J3e9H087mU/pjw8q4v3x9Uu9+6rQyPDc5T+rk/OoxaLWd0ToG0+tU6woVvgpfy/Gcx3co61+Y6d+XnTiMIwlYeROocRL5SetrIdSTZaM0mc3SfP4r3V6tnai7E9XflNv1e27c2WQfU58OENZy96XZYa3cyXJG6/HQoaS1rva1sah2TOfxQY8cTcA6XCfzMlv169dtOj9Zi1XF+VuXxderd7OXWbn3ev7X4rpC7W2ecVoIvXZVX03TdYML276yCFqP52iNTWmxC3Uuqksnr76RbGaVpeMJWPvuZJbnVpXPVj3kueoynRXXvinO37qb5kP0jlRerLLt1efruf/RMF5tuDJ6oX/ni60qLmybh6y2KT4PWle3gha7sINv+vLKqyMkLjLK0vEErH11Mk+HAZfnVr2ynK0qzus5vbhL0720wqqTXDteZHXbleKDb12y+8OZHU2LWzStfcO0qWXQcjI8cYoZ1uJ8wosjmE0+EIdhKXFEAWuX9/vLPc1YlRwGfMiD1eXKbNW+VZ3PcnL1T+vDhNsuqRB+XbDTjzUuBntoj98wLQ75dulwH0+Gn6WJlAWD4iKjPDmigLUrxTlW22esTvNgdYBc9aLyAo7n6XOri2GO0j+bLtZ5fxl/XbAOV1Xft+ndRTotrqZ90yVoFRcqLWazOl4OHtitlVNQXGSUJwLWisZ7HovDgcU5Vm9DxuIcqwPNWJWpuoBjm1msbPy5/PDgw00628n1iXZ7vli8abq7zoPWWbfblpyc3wpZ0GvOc+MtAaut0STNFocDl/9ecV/MWhXnWC3/3Q/Lb7pt7ASKm+iO6x+Cy8Zvbri7UBwOPd3VrUuibsq9Z9PH25Z86BC0FiGr882jgd1ykVFeCFgrak/t5uFqflt+HasiXF30ZNbqreVJ2JvG+JOr9CsPWVWn/WSjcR4ur96+/2LmqjgcuvxnKxUni9a7KXdPdQxaxc2jZSzoo6cvElRfmJjjIWA9q7nnkeXhYtNtbRbnHfW9dRUdwZZzg/KQVZxcXRySWr/+5dOFUn/droerh3R/c5Y+RMxcVV1OI1++f/Z1tKyYpRzn62H5zzDPQeuy4TcOT9LV3t48AF0cUcDK0tZ7CD/8TN8r00EeMMpmbhYe0k3kNQl26uncoE3fLjxZHJL6VVxpvLidz7LeXii1CFaX6Szfa7vY427b+ecGhzJby9L483me5z6nnR2VnN4tv3F4U//6WeefzWKxM1nxhZ2ivTvnDzozg/WkxsXhNp7UXagV0HqkmIl7DovFZSTO0mUx0Ocj/cNDXovH1xSPP9wvQtXishOLYBV9rlmNm9PuYxYr+zs93nN69/cVm95dLw7d1jtseJL6fjkwhurlW8GNLxAMlJrvv7L5eJbvJ200mefjZ8nfdalR/qyb5TtsJX+zWtv/fjbOSv6mQY22PXud5WtQ+Wu9rP5drOtule9FL5dtm90u9/PHMRmV/v/nysaLdRn1+dR671XL9B5quV63mo3nWdnf9qqq+rrCbD7Oyv52v/Wy7e27T9jetz7qXz+lVFUdzwzW1psI36fKHbYh3oS4THFe0fMJ+g/p5qx/V3eefv+54dDlqobfemwiX0dPp9nV3ZM//yvv/gNMr0/Tjm5LCZutfCv44eaLK75DgKMJWNnff244d6pmh7LlHnmD8Spc5e6/9vMbL9Pv6Weto2WP33qMDVmjNHn+EkON4P2k802zX9xdtLxxNLQ0+ufpdIGH9HNQ5zpAf/U0YEVfUDJLfz+eUFOiXoey7ZYwhd7fJ2/57ceXtdDnk/Kn6XuthJULDVlZGs9uX86zu//RYE/+PAVNYuXu0petV96HQCsztoM7lxR67DhmsJ5PWC4RNYvTcQajKsB1kweHb5u+/dhP0+uv9WdxFiFr0vHbdUW4+pVerp3aPIBGfrvx1d3517jXGXFWZ2yLfPU9+Esr719xXcDJbOUb1zP3EOVRTwNW7DelNh8evE+XYbd0aXs/vyzfgZylX2VXRV9x2mVKb/TPSnB40verojecxTk5T1e/5mk2qb5Q6mtZ3kFO0my+Gq5ybYJ35Lcbt1xw1b3OiLE2Y9vrw4P9vE1WcVmL4rqAr+7oceIeorwoPft9t1XjmzVR3xDa8m2kRt/8qviW36Nm3wbK93zm+Z5PLRu/pZiN5uPxtm+VbV/XeSCZj7KO34DcWdX5BtYGs8livWRl7y1/rFj3440rv8Hn+Gb7Cvq208bt7Ui+TVWnvQ3iW4R1viF3mG8RvlnFB1uf/V1HW6vGNtr52+Vq6FX64I6r3sAZ8dX3zeNUw6+61/na+EKNjqAIRWuD+2xW8ewlnV8REhZ/te291F7uR8VyvKrJZD4pqSK8jEYrVYSWosqWoUs1XP4IjTrFsuXLP4+u62HT5RqOpcPuw6U6YqpOeIjp65pU2fo92LZVs43vex1tr7o7fy4vceRV+uCOaz8b58ZOutWeWrPZlMeZodd/Xz5jNcs7jrxjq7E3tPi95XONVp5oa8dzgICykIezyWIGqWSZmlad2YwoUcG7U8jaNCgfT2ddL2D1cFZjvXoXHl73HS8OuC5rtu9+7VzUC86FfgVDtecqfXDH1SCszPJBpUXDzzZPXbUfpKIH+tX31vK5Z5WBoH5HsCuzlp/ham3+PAO1Cd7bBtCW73vTWz2ejrp+/9D7dVJ3u93D4bmtpyQc7PDglqMM65ru/OyyGuy4ClhHXaUP7riazQblrT/fSOvuvWzaQ8t1HuibLvcW6x1am5mmmh3OPrJJtSafYXkVIStq9b/RdoCp/NyWM3llf/umNm+7XdfdoKrJBnvAYFBdDfuLRf8Uc5h9cbg+r9HyXMOqxej74cFHfZqxrLvjOoBZVrXLKn1wx1XV8RTn+JT8wvNhp/XOoOhMHs9r2vi0YR1x9xmh8s6sYWfc6BBUMXAv/+7AOgeFvEPeuBfeVpdto8EAURw2Hufv/81h02IgHG8Kj0fWQbfc0ehdyCr6o+jtdGcOtI3l66hxWw6YDY+puv11vrylf6+OpEof3HFVB6xio4yZseg+c/Kmio5h+ezNbO/I6p13UvQx7abKs42D+D5FdOZbZikbCdg22gSCmt6ex/de62UHqb1NO1/7re7v4wD2PAtYuTNcw9O3nw8arGvstR7Ll1LUxip9cMdVL2Atfrd1h9Xk0EybajbIF6Goelkq1ku+91bMgJT/bc0qZoCWT3cwUR16MfPTctuo93nUqOeAtbJn/TQj1WbRCstDRW9e673UPqdTdxoeDn9+Y4SdhoAd7oCU2ff5Ttt2itvuCKt3VaUP7rgaBKyVvynOJ5gUez5lI1dxSYF8YCpC1V73+p8H0/VlelyexbWYyv5uYy2fb/kshcXzFIeWSn+/Wa0GkqfO6PF8jadLLhTr+OlyDI/rerVixB+SeNwrfvwc3i7l47JPdnHNr+UAsm2QWqzb/DOt2nabbytKqUPXmy8PFH1N9FETNcj6sPxhz9ZvS7LuPl1+uHBH92D53tbzFeMfbs7SaeSdnrNseZuYT+nTp5Q+/vVX+uP0NJ2flH/I4a8PAD3zJnXtvtrMYKku9Woqe69T129n5BZMnyullHrHdRw3ez5yxb32Xu51GHn/xTqm6e76Ip1ert26+fTjcsYLAN4fAeu9K8LV6t3yb74c5tDr3Zf06t7Ns3/dtR+Ad0vAes+ycZqthKti9urrwc57mqZ/Z8sfAeCdE7DerSyNv12lV6eY3//ozRcHHv77vfwJAN4fAeudysbf3nxL87ChZpT+ep5Me0g/vztACMD7JWC9R9k4fSu5Bsbs3wOGmtFf6SVf/UzyFQDvmYD1Do3+WTs0uHT+12j5076N0mTlXLD7r9dOcAfgXROw3ptsnD6vnte+6vxzGu/92gjFRWVvV2avbtIXV5AF4J0TsN6bT3+Uzl49OklXv2b7C1nZKE1eXbH/Id38z+wVAO+fgHV0ipA1T7PJaKcX+iwubjr7dZvOV9Le/eVpcnccAI6BgPXe3P1Ia9dML3Vyfpt+zYugNU6jLC5qZaNxmszmi4ubrs6k3V+epb1eQB4ADuhAN3sepcl85bycN9zsuZPRJM1fXWC0noeH+zSb/Zd+/Pg3/f79eEmH6XTDlNPy5s6fPn1KHz/+lf78Mw9UG45NPoYrU1cAHJc3NyjcfY3mk+U9f8vN5uOs7O9U3Xp1c+eD8TkqpZQ6zjrMIcLsYzpd/ljuJP3xafkjrUyvT9PZ5X1avf3fXj3cpMsz51wBcLxKk9cuq9bsymRU+reqYWWj+Xiyz9msWf7RZeXLopRSSh1PlT64u8rG+RBcj4E6sPKgNZntMmg9Bqus7LWVUkqp46vSB4MrH3hbzqTMZpP5uBi4ncsTU1k2H40ntUNuldlkPB/lz1n6WkoppdSR1k6/RZiPv8ufIj2kG+f2BMlSNvqU/v74V/rjz9PH8+JOTkovVPrwUJzNNVt8y/C/H/+m77/v0qYvGALAsTvQZRoAAN4vFxoFAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAACCCVgAAMEELACAYAIWAEAwAQsAIJiABQAQTMACAAgmYAEABBOwAABCpfT/Iai8QIcb0isAAAAASUVORK5CYII=";
    private ComicService comicService;
    @Autowired
    public void setComicService(ComicService comicService) {this.comicService = comicService;}

    @Autowired
    UserService userService;

    @Autowired
    private JwtUtil jwtUtil;
    private ChapterService chapterService;
    @Autowired
    public void setChapterService(ChapterService chapterService) {this.chapterService = chapterService;}

    /* Return a JSON object with
        comicId
        coverArt
        title
        chapterArray
        tagArray
    */
    @RequestMapping(value = "{id}", produces = "application/json")
    public ResponseEntity<String> getComicTitleAndCoverById(@PathVariable(value = "id") Long comicId) throws JSONException {
        Comic comic = this.comicService.findById(comicId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("comicid", comicId);
        jsonObject.put("title", comic.getTitle());
        JSONArray chapterArray = new JSONArray();
        JSONArray tagArrayJson = new JSONArray();


        List<Chapter> chapterList = comic.getChapters().stream().collect(Collectors.toList());
        System.out.println(chapterList);
        Collections.sort(chapterList, Comparator.comparingInt(Chapter::getChapterindex));
        //Collections.reverse(chapterList);

        for( Chapter chapter: chapterList){
            JSONObject chapterJson = new JSONObject();
            try {
                chapterJson.put("chapterTitle", chapter.getName());
                chapterJson.put("chapterId", chapter.getChapterid());
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            chapterArray.put(chapterJson);
        }
        comic.getTags().stream().forEach( tag -> {
            JSONObject chapterObj = new JSONObject();
            try {
                chapterObj.put("tagid", tag.getTagid());
                chapterObj.put("tagname", tag.getTagname());
                tagArrayJson.put(chapterObj);
            } catch (JSONException e) {

            }
        });
        jsonObject.put("chapters", chapterArray);
        jsonObject.put("tags", tagArrayJson);
        jsonObject.put("coverart", getCoverArtById(comicId).getString("coverart") );
        jsonObject.put("status", comic.getStatus());
        jsonObject.put("pagecount", comic.getPagecount());
        jsonObject.put("uploader", comic.getUser().getEmail());
        jsonObject.put("created", comic.getCreatedat());
        jsonObject.put("modified", comic.getLastupdated());
        return ResponseEntity.ok(jsonObject.toString());
    }

    /*
    Returns a JSONarray with the pages of a given chapter
    */
    @RequestMapping(value = "{comicId}/{chapterId}", produces = "application/json")
    public ResponseEntity<String> getChapterPages(@PathVariable(value = "comicId") Long comicId, @PathVariable(value = "chapterId") Long chapterId ) throws JSONException {
        JSONObject response = new JSONObject();
        JSONArray pagesJsonArray = new JSONArray();
        this.comicService.findById(comicId).getChapters().stream().filter(chapter -> chapter.getChapterid()==chapterId).forEach( chapter -> {
            List<Page> pageSet = chapter.getPage();
            pageSet.stream().forEach( page -> {
                JSONObject pageObj = new JSONObject();
                try {
                    pageObj.put("hash",page.getHash());
                    pageObj.put("pageid",page.getPageid());
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                pagesJsonArray.put(pageObj);
            });
        });
        response.put("pages",pagesJsonArray);
        return ResponseEntity.ok(response.toString());
    }

    //Return JsonObj {comicid, pagehash, title} first page of a comic alias coverart
    public JSONObject getCoverArtById(Long comicId) throws JSONException {
        JSONObject coverArtJson = new JSONObject();
        try {
            Page coverArtPage = this.comicService.findById(comicId).getChapters()
            .stream()
            .filter(chapter -> chapter.getChapterindex().equals(0))
            .collect(Collectors.toSet()).iterator().next().getPage()
            .stream()
            .filter(page -> page.getPageindex().equals(0))
            .collect(Collectors.toSet()).iterator().next();


            coverArtJson.put("comicid", comicId);
            coverArtJson.put("title", this.comicService.findById(comicId).getTitle());
            coverArtJson.put("coverart", coverArtPage.getHash());
        }catch (Exception e){
            JSONObject errorJson = new JSONObject();
            errorJson.put("comicid", comicId);
            errorJson.put("title", this.comicService.findById(comicId).getTitle());
            errorJson.put("coverart", NO_COVER_ART );
            return errorJson;
        }
        return coverArtJson;
    }

    @RequestMapping(value = "/random" )
    public ResponseEntity<String> getRandomComicId() throws JSONException {
        List<Long> comicIds = this.comicService.findAll().stream().map( comic -> comic.getComicid()).collect(Collectors.toList());
        Random rn = new Random();
        int randomMemberofList = (rn.nextInt(comicIds.size()) + 0);
        return  ResponseEntity.ok(comicIds.get(randomMemberofList).toString());
    }

    @RequestMapping(value = "/popularUpdatedNew", produces = "application/json")
    public ResponseEntity<String> popularUpdatedNew() throws JSONException {

        JSONArray mostViewedComicJSONArray = new JSONArray();
        JSONArray mostRecentComicsJSONArray = new JSONArray();
        JSONArray lastUpdatedComicsJSONArray = new JSONArray();
        JSONObject popularUpdatedNewJSON = new JSONObject();

        List<Comic> mostViewedComicList = this.comicService.mostViewedComics();
        for( int i = 0; i < 3; i++ ) {
            mostViewedComicJSONArray.put(this.getCoverArtById(mostViewedComicList.get(i).getComicid()));
        }
        List<Comic> mostRecentComicsList = this.comicService.mostRecentComics();
        for( int i = 0; i < 3; i++ ) {
            mostRecentComicsJSONArray.put(this.getCoverArtById(mostRecentComicsList.get(i).getComicid()) );
        }

        List<Comic> lastUpdatedComicsList = this.comicService.lastUpdatedComics();
        for( int i = 0; i < 3; i++ ) {
            lastUpdatedComicsJSONArray.put(this.getCoverArtById(lastUpdatedComicsList.get(i).getComicid()) );
        }

        popularUpdatedNewJSON.put("mostViewed", mostViewedComicJSONArray);
        popularUpdatedNewJSON.put("mostRecent", mostRecentComicsJSONArray);
        popularUpdatedNewJSON.put("lastUpdated", lastUpdatedComicsJSONArray);
        return ResponseEntity.ok(popularUpdatedNewJSON.toString());
    }

    /*
    Return a JSON array with Pages of the given chapter of a given comic
    */
    @RequestMapping(value = "/pages/{chapterId}", produces = "application/json")
    public ResponseEntity<String> getChapterPagesByComicIdAndChapterId( @PathVariable(value = "chapterId") Long chapterId )  {
        JSONArray pageJsonArray = new JSONArray();
        List<Page> pageList = this.chapterService.findById(chapterId).getPage().stream().collect(Collectors.toList());
        Collections.sort(pageList, new Comparator<Page>() {
            @Override
            public int compare(Page o1, Page o2) {
                return o1.getPageindex() - o2.getPageindex();
            }
        });
        pageList.stream().forEach( page -> {
            JSONObject pageJsonObj = new JSONObject();
            try {
                pageJsonObj.put("pageId", page.getPageid());
                pageJsonObj.put("hash", page.getHash());
                pageJsonObj.put("pageIndex", page.getPageindex());
                pageJsonArray.put(pageJsonObj);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });
        return ResponseEntity.ok(pageJsonArray.toString());
    }

    @RequestMapping(value = "/chapters/{comicId}", produces = "application/json")
    public ResponseEntity<String> getChaptersByComicId( @PathVariable(value = "comicId") Long comicId ) {
        JSONArray jsonArray = new JSONArray();
        List<Chapter> chapterList = this.comicService.findById(comicId).getChapters().stream().collect(Collectors.toList());
        Collections.sort(chapterList, Comparator.comparingInt(Chapter::getChapterindex));
        Collections.reverse(chapterList);

        for( Chapter chapter: chapterList){
            JSONObject chapterJson = new JSONObject();
            try {
                chapterJson.put("chapterTitle", chapter.getName());
                chapterJson.put("chapterId", chapter.getChapterid());
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            jsonArray.put(chapterJson);
        }

        /*chapterList.stream().forEach( chapter -> {
            JSONObject chapterJson = new JSONObject();
            try {
                chapterJson.put("chapterTitle", chapter.getName());
                chapterJson.put("chapterId", chapter.getChapterid());
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            jsonArray.put(chapterJson);
        });*/

        return ResponseEntity.ok(jsonArray.toString());
    }

    @RequestMapping(value = "/page/{page}/{size}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> page( @PathVariable(value = "page") int page, @PathVariable(value = "size") int size  ) throws JSONException {
        Pageable firstPageWithTwoElements = PageRequest.of(page, size);
        org.springframework.data.domain.Page<Comic> comicList = this.comicService.pageable(firstPageWithTwoElements);

        JSONObject response = new JSONObject();
        JSONArray comicJson = new JSONArray();
        comicList.forEach(Comic->{
            try {
                comicJson.put(
                    this.getCoverArtById(Comic.getComicid())
                );
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });
        response.put("page", page);
        response.put("size", size);
        response.put("totalElements", comicList.getTotalElements());
        response.put("totalPages", comicList.getTotalPages());
        response.put("comics", comicJson);

        return ResponseEntity.ok(response.toString());
    }

    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> searchComic(@RequestBody SearchDTO searchFormValue, @PathVariable(value = "page") int page, @PathVariable(value = "size") int size ) throws JSONException {

        Sort sort = Sort.by("title").descending();

        List<String> comicStatusList = new ArrayList<>() {{
                add("Cancelled");
                add("Completed");
                add("Incomplete");
                add("OnHiatus");
            }
        };

        Map<String, Sort> sortMap = new HashMap<>();
        sortMap.put("viewCountASC", Sort.by("view").ascending());
        sortMap.put("viewCountDSC", Sort.by("view").descending());
        sortMap.put("titleASC", Sort.by("title").ascending());
        sortMap.put("titleDSC", Sort.by("title").descending());
        sortMap.put("pageCountASC", Sort.by("pagecount").ascending());
        sortMap.put("pageCountDSC", Sort.by("pagecount").descending());

        if( !(searchFormValue.getStatus() == null) ){
            comicStatusList = searchFormValue.getStatus();
        }

        if( searchFormValue.getOrderBy() != null){
            sort = sortMap.get(searchFormValue.getOrderBy());
        }

        List<Long> tagIdList = new ArrayList<>();


        if( searchFormValue.getIncludedTags().isEmpty() ){
            tagIdList.add(1L);
        } else {
            for(Tag tag: searchFormValue.getIncludedTags()){
                tagIdList.add(tag.getTagid());
            }
        }



        Long viewCountTo = searchFormValue.getViewCountTo();
        Long viewCountFrom = searchFormValue.getViewCountFrom();


        if( viewCountFrom == null && viewCountTo != null ) {
            viewCountFrom  = Long.MIN_VALUE;
        }

        else if( viewCountTo == null && viewCountFrom != null) {
            viewCountTo = Long.MAX_VALUE;
        }

        else if(
            (viewCountTo == null && viewCountFrom == null) ||
            viewCountFrom > viewCountTo ||
            (viewCountTo < 0 || viewCountFrom < 0)
        ){
            viewCountFrom  = Long.MIN_VALUE;
            viewCountTo = Long.MAX_VALUE;
        }


        Long pageCountFrom = searchFormValue.getPageCountFrom();
        Long pageCountTo = searchFormValue.getPageCountTo();


        if( pageCountFrom == null && pageCountTo != null ) {
            pageCountFrom  = Long.MIN_VALUE;
        }

        else if( pageCountTo == null && pageCountFrom != null) {
            pageCountTo = Long.MAX_VALUE;
        }

        else if(
                (pageCountTo == null && pageCountFrom == null) ||
                        pageCountFrom > pageCountTo ||
                        (pageCountTo < 0 || pageCountFrom < 0)
        ){
            pageCountFrom  = Long.MIN_VALUE;
            pageCountTo = Long.MAX_VALUE;
        }

        Date fromDate = searchFormValue.getFromDate();
        Date toDate = searchFormValue.getToDate();

        if( fromDate == null && toDate != null ) {
            fromDate  = new Date(0);
        }

        else if( toDate == null && fromDate != null) {
            toDate = new Date(System.currentTimeMillis());
        }

        else if(
            (toDate == null && fromDate == null) ||
            ( fromDate.before(toDate) )
        ){
            fromDate  = new Date(0);
            toDate = new Date(System.currentTimeMillis());
        }

        JSONObject response = new JSONObject();
        JSONArray comicJson = new JSONArray();

        org.springframework.data.domain.Page<Comic> comicList = this.comicService.searchComic(
            tagIdList,
            fromDate,
            toDate,
            viewCountFrom,
            viewCountTo,
            pageCountFrom,
            pageCountTo,
            comicStatusList,
            PageRequest.of(page, size, sort)
        );

        comicList.forEach(Comic->{

            System.out.println(
                    "id " + Comic.getComicid() + " " +
                    "title " + Comic.getComicid() + " " +
                    "status " + Comic.getStatus() + " " +
                    "view " + Comic.getView() + " " +
                    "pagecount " + Comic.getPagecount()
            );

            try {
                comicJson.put(
                        this.getCoverArtById(Comic.getComicid())
                );
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });
        response.put("page", page);
        response.put("size", size);
        response.put("totalElements", comicList.getTotalElements());
        response.put("totalPages", comicList.getTotalPages());
        response.put("comics", comicJson);
        return ResponseEntity.ok(response.toString());
    }

    @RequestMapping(value = "/usercomic", method = RequestMethod.GET)
    public ResponseEntity<?> getUserComic(
        @RequestHeader("Authorization") String jwtToken
    ){
        User user = userService.getUserFromJwtToken(jwtToken);
        if( user != null ) {
            JSONArray userComicJSONArray = new JSONArray();
            user.getComics().forEach(comic -> {
                try {
                    userComicJSONArray.put(
                            getCoverArtById(comic.getComicid())
                    );
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            });
            return ResponseEntity.status(200).body(userComicJSONArray.toString());
        }
        return ResponseEntity.status(403).build();
    }

    @RequestMapping(value = "/deletecomic/{comicId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUserComic(
            @RequestHeader("Authorization") String jwtToken,
            @PathVariable(value = "comicId") Long comicId
    ){
        if( userService.comicBelongsToUser(comicId,jwtToken)){
            comicService.deleteComic( comicId );
            return ResponseEntity.status(200).body("Comic with id: "+comicId +" deleted!");
        }
        return ResponseEntity.status(403).build();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> saveComic(
        @RequestBody ComicDTO comicDTO,
        @RequestHeader("Authorization") String jwtToken
    ){
        User user = userService.getUserFromJwtToken(jwtToken);
        if( user == null){
            return ResponseEntity.status(403).build();
        }

        Long pageCount = 0L;
        Comic comicToSave = new Comic();
        comicToSave.setUser( user );
        comicToSave.setTitle(comicDTO.getComicTitle());
        comicToSave.setTags(comicDTO.getTags());
        List<Chapter> chaptersToSave = new ArrayList<>();


        List<Chapter> chapterArrayFromDTO = comicDTO.getChapters();
        for (int i = 0; i < chapterArrayFromDTO.size(); i++) {
            Chapter tmpChapter = new Chapter();
            tmpChapter.setComic(comicToSave);
            tmpChapter.setName(chapterArrayFromDTO.get(i).getName());
            tmpChapter.setChapterindex(i);
            List<Page> pageArrayFromDtO = chapterArrayFromDTO.get(i).getPage();
            List<Page> pageToAdd = new ArrayList<>();
            for (int j = 0; j < pageArrayFromDtO.size(); j++) {
                Page tmpPage = new Page();
                pageCount++;
                tmpPage.setChapter(tmpChapter);
                tmpPage.setPageindex(j);
                tmpPage.setHash(pageArrayFromDtO.get(j).getHash());
                pageToAdd.add(tmpPage);
            }
            tmpChapter.setPage(pageToAdd);
            chaptersToSave.add(tmpChapter);
        }
        comicToSave.setChapters(chaptersToSave);
        comicToSave.setStatus(comicDTO.getState());
        comicToSave.setView(0L);
        comicToSave.setPagecount(pageCount);
        comicToSave.setCreatedat( new Date( System.currentTimeMillis()));

        this.comicService.saveComic(comicToSave);

        return ResponseEntity.status(200).build();
    }


    @RequestMapping(value = "/edit/{comicId}", method = RequestMethod.GET)
    public ResponseEntity<?> getComicDataForEdit(
            @PathVariable(value = "comicId") Long comicId,
            @RequestHeader("Authorization") String jwtToken
    ) throws JSONException {
        if(userService.comicBelongsToUser(comicId,jwtToken)){
            JSONObject comicJSON = new JSONObject();
            JSONArray tagJSONArray = new JSONArray();
            JSONArray chapterJSONArray = new JSONArray();
            Comic comic = this.comicService.findById(comicId);
            comicJSON.put("comicTitle", comic.getTitle());
            comicJSON.put("status", comic.getStatus());
            comic.getTags().stream().forEach(tag -> {
                JSONObject tagJSON = new JSONObject();
                try {
                    tagJSON.put("tagid", tag.getTagid().toString());
                    tagJSON.put("name", tag.getTagname());
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                tagJSONArray.put(tagJSON);
            });
            comicJSON.put("tagArray",tagJSONArray);
            comic.getChapters().forEach( chapter -> {
                JSONObject chapterJSON = new JSONObject();
                Long chapterId = chapter.getChapterid();
                JSONArray pageArrayJSON = new JSONArray();
                chapter.getPage().forEach(page -> {
                    JSONObject tmpPage = new JSONObject();
                    try {
                        tmpPage.put("hash", page.getHash());
                        tmpPage.put("index", page.getPageindex());
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    pageArrayJSON.put(tmpPage);
                });
                try {
                    chapterJSON.put("chapterId", chapterId);
                    chapterJSON.put("chapterTitle", chapter.getName());
                    chapterJSON.put("pageArray", pageArrayJSON);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                chapterJSONArray.put(chapterJSON);
            });

            comicJSON.put("chapterArray",chapterJSONArray);
            return ResponseEntity.status(200).body(comicJSON.toString());
        }
        return ResponseEntity.status(403).build();
    }

    @RequestMapping(value = "/edit/{comicId}", method = RequestMethod.POST)
    public ResponseEntity<?> editComic(
        @PathVariable(value = "comicId") Long comicId,
        @RequestHeader("Authorization") String jwtToken,
        @RequestBody ComicDTO comicDTO
    ){
        if(userService.comicBelongsToUser(comicId,jwtToken)){
            Comic modifiedComic = comicService.findById(comicId);
            List<Chapter> dtoChapters = comicDTO.getChapters();
            List<Chapter> chaptersToAdd = new ArrayList<>();
            Long pageCount = 0L;
            for(int i = 0; i < dtoChapters.size(); i++){
                Chapter chapterToAdd = new Chapter();
                chapterToAdd.setComic(modifiedComic);
                chapterToAdd.setName(dtoChapters.get(i).getName());
                chapterToAdd.setChapterindex(i);


                List<Page> actChapterPageArray = dtoChapters.get(i).getPage();
                List<Page> pageToAddList = new ArrayList<>();
                for (int j = 0; j < actChapterPageArray.size(); j++) {
                    Page pageToAdd = new Page();
                    pageCount++;
                    pageToAdd.setChapter(chapterToAdd);
                    pageToAdd.setPageindex(j);
                    pageToAdd.setHash(actChapterPageArray.get(j).getHash());
                    pageToAddList.add(pageToAdd);
                }
                chapterToAdd.setPage(pageToAddList);
                chaptersToAdd.add(chapterToAdd);
            }
            modifiedComic.setTitle(comicDTO.getComicTitle());
            modifiedComic.setChapters(chaptersToAdd);
            modifiedComic.setStatus(comicDTO.getState());
            modifiedComic.setTags(comicDTO.getTags());
            modifiedComic.setPagecount(pageCount);
            modifiedComic.setLastupdated(( new Date( System.currentTimeMillis())));
            comicService.saveComic(modifiedComic);
            return ResponseEntity.status(200).build();
        }
        return ResponseEntity.status(403).build();
    }
}