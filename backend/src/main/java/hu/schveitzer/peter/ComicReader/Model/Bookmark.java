package hu.schveitzer.peter.ComicReader.Model;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "bookmark")
public class Bookmark {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookmarkid;

    private Long comicid;

    private Long chapterid;

    private Long pageid;


    @ManyToOne
    @JoinColumn(name="userid", nullable=false)
    private User user;

}
