package hu.schveitzer.peter.ComicReader.Model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userid;



    private String email;

    private String password;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    private Set<Comic> comics;


    @OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    private Set<Bookmark> bookmarks;

}
