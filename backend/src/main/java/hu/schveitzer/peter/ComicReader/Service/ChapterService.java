package hu.schveitzer.peter.ComicReader.Service;

import hu.schveitzer.peter.ComicReader.Model.Chapter;
import hu.schveitzer.peter.ComicReader.Repository.ChapterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChapterService {
    @Autowired
    private ChapterRepo chapterRepo;

    public Chapter findById( long chapterId){
        return chapterRepo.findById(chapterId);
    }
}
