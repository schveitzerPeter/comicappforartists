package hu.schveitzer.peter.ComicReader.DTO;

import hu.schveitzer.peter.ComicReader.Model.Chapter;
import hu.schveitzer.peter.ComicReader.Model.Tag;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ComicDTO {
    List<Chapter> chapters;
    String comicTitle;
    String state;
    Set<Tag> tags;
}
