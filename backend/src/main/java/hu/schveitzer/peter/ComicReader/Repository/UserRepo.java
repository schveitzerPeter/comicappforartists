package hu.schveitzer.peter.ComicReader.Repository;

import hu.schveitzer.peter.ComicReader.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {

    User findByEmail(String email);

    User save( User user);
}
