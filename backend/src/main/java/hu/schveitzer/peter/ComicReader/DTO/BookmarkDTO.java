package hu.schveitzer.peter.ComicReader.DTO;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@ToString
@Data
public class BookmarkDTO {
    private Long comicId;
    private Long chapterId;
    private Long pageId;
}