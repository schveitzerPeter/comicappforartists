package hu.schveitzer.peter.ComicReader.Repository;

import hu.schveitzer.peter.ComicReader.Model.Chapter;
import hu.schveitzer.peter.ComicReader.Model.Comic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ChapterRepo extends JpaRepository<Chapter, Long>, JpaSpecificationExecutor<Comic> {
    Chapter findById( long id);
}
