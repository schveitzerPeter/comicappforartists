package hu.schveitzer.peter.ComicReader.Model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "page")
public class Page {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pageid;

    @ManyToOne
    @JoinColumn(name="chapterid", nullable=false)
    private Chapter chapter;

    @Column(columnDefinition = "LONGTEXT")
    private String hash;

    private Integer pageindex;
}
