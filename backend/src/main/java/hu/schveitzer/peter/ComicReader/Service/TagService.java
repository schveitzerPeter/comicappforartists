package hu.schveitzer.peter.ComicReader.Service;

import hu.schveitzer.peter.ComicReader.Model.Tag;
import hu.schveitzer.peter.ComicReader.Repository.TagRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class TagService {

    @Autowired
    TagRepo tagRepo;

    public Optional<Tag> findById(Long id){
        return tagRepo.findById(id);
    }

    public Set<Tag> findTagByTagnameContaining(String subString){
        return tagRepo.findTagByTagnameContaining(subString);
    }

    public Tag saveTag(Tag tag){
        return tagRepo.save(tag);
    }
}
