package hu.schveitzer.peter.ComicReader.Service;

import hu.schveitzer.peter.ComicReader.Model.Bookmark;
import hu.schveitzer.peter.ComicReader.Model.User;
import hu.schveitzer.peter.ComicReader.Repository.BookmarkRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookmarkService {

    @Autowired
    private BookmarkRepo bookmarkRepo;

    public List<Bookmark> getBookmarksByUserid(Long userId){
        return this.bookmarkRepo.getBookmarkByUser(userId);
    }

    public Bookmark saveBookmark(Bookmark bookmark){
        return this.bookmarkRepo.save(bookmark);
    }

    @Transactional
    public void deleteBookmarkById( Long userId){ bookmarkRepo.deleteBookmarkById(userId);}
}
