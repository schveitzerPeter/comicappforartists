package hu.schveitzer.peter.ComicReader.DTO;



public class CheckEmailDTO {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CheckEmailDTO(String email) {
        this.email = email;
    }

    public CheckEmailDTO() {
    }
}
