package hu.schveitzer.peter.ComicReader.Controller;


import com.google.common.hash.Hashing;
import hu.schveitzer.peter.ComicReader.DTO.CheckEmailDTO;
import hu.schveitzer.peter.ComicReader.DTO.LoginDTO;
import hu.schveitzer.peter.ComicReader.DTO.UserRegistrationDTO;
import hu.schveitzer.peter.ComicReader.Model.User;
import hu.schveitzer.peter.ComicReader.Service.UserService;
import hu.schveitzer.peter.ComicReader.Util.JwtUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping(path = "api/v1/user/")
@CrossOrigin(origins = "http://localhost:4200", methods = {RequestMethod.GET,RequestMethod.POST})
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    public void setUserService(UserService userService) {this.userService = userService;}

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {this.authenticationManager = authenticationManager;}

    public void setUserDetailsService(UserDetailsService userDetailsService) {this.userDetailsService = userDetailsService;}

    public void setJwtUtil(JwtUtil jwtUtil) {this.jwtUtil = jwtUtil;}

    @RequestMapping(value = "checkemail", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<String> checkIfEmailExistInDatabase(@RequestBody CheckEmailDTO email) throws JSONException {
        return (userService.checkIfEmailExistInDatabase(email.getEmail()) == null) ? ResponseEntity.ok("false") : ResponseEntity.ok("true");
    }

    @RequestMapping(value = "register", method = RequestMethod.POST, produces = "text/plain")
    public ResponseEntity<String> registerUser(@RequestBody UserRegistrationDTO user) throws JSONException {
        JSONObject response = new JSONObject();
        User userToSave = new User();
        userToSave.setPassword(
            bCrypt( user.getPassword() )
        );
        userToSave.setEmail(user.getEmail());
        userService.saveUser( userToSave );
        return ResponseEntity.ok(response.toString());
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResponseEntity<String> loginUser(@RequestBody LoginDTO user) throws Exception {
        try {
            this.authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getEmail(), bCrypt( user.getPassword() )
                    )
            );
        } catch (BadCredentialsException e){
            return ResponseEntity.status(403).body("bad cred");
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        final String jwt = this.jwtUtil.generateToken(user.getEmail());
        JSONObject tokenJson = new JSONObject();
        tokenJson.put("token", jwt);
        return ResponseEntity.ok(tokenJson.toString());
    }

    @RequestMapping(value = "auth", method = RequestMethod.GET)
    public ResponseEntity<String> userAuth(
        @RequestHeader("Authorization") String jwtToken
    ) throws Exception {
        if(userService.getUserFromJwtToken(jwtToken)==null){
            return ResponseEntity.status(403).build();
        } else {
            return ResponseEntity.status(200).build();
        }
    }

    public static String bCrypt(String data) {
        return Hashing.sha256().hashString(data, StandardCharsets.UTF_8).toString();
    }
}