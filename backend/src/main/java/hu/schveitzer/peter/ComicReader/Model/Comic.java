package hu.schveitzer.peter.ComicReader.Model;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "comic")
@EqualsAndHashCode
public class Comic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long comicid;

    @OneToMany(mappedBy="comic", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Chapter> chapters = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="userid", nullable=false)
    private User user;

    @ManyToMany()
    @JoinTable(name = "comic_tag",
            joinColumns = { @JoinColumn(name = "comicid")},
            inverseJoinColumns = { @JoinColumn (name = "tagid")}
    )
    private Set<Tag> tags;

    @Column(columnDefinition = "varchar(255) default 'Title'")
    private String title;

    @Column(columnDefinition = "bigint(255) default 0")
    private Long view;

    @Column(columnDefinition = "date default '2022-01-01'")
    private Date createdat;

    @Column(columnDefinition = "date default '2022-01-01'")
    private Date lastupdated;

    @Column(columnDefinition = "varchar(255) default 'Incomplete'")
    private String status;

    @Column(columnDefinition = "bigint(255) default 0")
    private Long pagecount;

    public void setChapters( List<Chapter> chapters){
        this.chapters.clear();
        this.chapters.addAll(chapters);
    }
}
