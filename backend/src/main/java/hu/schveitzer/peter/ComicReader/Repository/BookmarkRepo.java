package hu.schveitzer.peter.ComicReader.Repository;

import hu.schveitzer.peter.ComicReader.Model.Bookmark;
import hu.schveitzer.peter.ComicReader.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookmarkRepo extends JpaRepository<Bookmark, Long> {

    @Query(value = "SELECT b FROM Bookmark b WHERE b.user.userid = :userid")
    List<Bookmark> getBookmarkByUser(Long userid);
    @Modifying
    @Query(value = "DELETE FROM Bookmark b WHERE b.bookmarkid = :bookmarkId")
    void deleteBookmarkById(Long bookmarkId);

    Bookmark save( Bookmark bookmark);
}
